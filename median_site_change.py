from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *

#claculate the median site change for different indices 
def median_site_change(current_result, proceding_result, index, utilityTmp):
    #current_result is current result set id, proceding_result is proceding result set id
    cursor = result_cnn.cursor()
    #for  LV
    site = []
    current = []
    proceding = []
    change = []
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, "Annual", current_result)
        if filtered_site:
            #find in current result set
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) AND " \
                          "index_id = (%%s) AND period = 'Annual' AND site_id in (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [current_result, index] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                current.append(float(row[0]))
                row = cursor.fetchone()

            #find in pervious result set
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) AND " \
                          "index_id = (%%s) AND period = 'Annual' AND site_id in (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [proceding_result, index] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                proceding.append(float(row[0]))
                row = cursor.fetchone()
            if len(proceding) == len(current):   #means both proceding and current have the same number of data
                for i in range(len(current)):
                        change.append(current[i] - proceding[i])

            if change:
                final_change = np.median(change)
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                              " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                              "%(value)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': current_result, 'utility_id': utilityTmp,
                                                     'index_id': index, 'statistic_id': "utility LV Median change",
                                                     'period': "Annual", 'value': float(final_change)})


                result_cnn.commit()
                another_cursor.close()


    # for MV
    site = []
    current = []
    proceding = []
    change = []
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, "Annual", current_result)
        if filtered_site:
            # find in current result set
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) AND " \
                          "index_id = (%%s) AND period = 'Annual' AND site_id in (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [current_result, index] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                current.append(float(row[0]))
                row = cursor.fetchone()

            # find in pervious result set
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) AND " \
                          "index_id = (%%s) AND period = 'Annual' AND site_id in (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [proceding_result, index] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                proceding.append(float(row[0]))
                row = cursor.fetchone()
            if len(proceding) == len(current):     #means both proceding and current have the same number of data
                for i in range(len(current)):
                        change.append(current[i] - proceding[i])
            if change:
                final_change = np.median(change)
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                              " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                              "%(value)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': current_result, 'utility_id': utilityTmp,
                                                     'index_id': index, 'statistic_id': "utility MV Median change",
                                                     'period': "Annual", 'value': float(final_change)})

                result_cnn.commit()
                another_cursor.close()