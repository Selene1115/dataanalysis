# THis file include NS SI PI calcluattion
__author__ = {'name' : 'Changze Huang'}
import numpy as np
import datetime
from database_connections import *

def dvolt2Pu(mvolt, siteTmp):
	instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
	cursor = another_cnn.cursor()
	select_stmt = "SELECT discrete_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"  # continous scaling factor from instrument database
	cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
	row = cursor.fetchone()
	dscalFactor = row[0]
	select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
	cursor.execute(select_stmt, {'site_id': siteTmp})
	row = cursor.fetchone()
	refVoltage = row[0]
	result = mvolt * dscalFactor / refVoltage
	another_cnn.commit()
	cursor.close()
	return result



def siteCoverage_not_write(startDate, endDate, siteTmp):
	#determine the time interval, then calculate how many intervals in the whole year - potential record
	#in the database, count how many intervals - actual record
	cursor = cnn.cursor()
	startDate = startDate + " 00:00:00"
	endDate = endDate + " 00:00:00"
	# find the time interval
	select_stmt = "SELECT time_stamp FROM continuous_voltage WHERE site_id = %(site_id)s AND " \
				  "time_stamp >= %(sDate)s AND time_stamp < %(eDate)s LIMIT 0, 4"
	cursor.execute(select_stmt, {'site_id': siteTmp, 'sDate': startDate, 'eDate': endDate})
	rows = cursor.fetchall()
	time = []
	for row in rows:
		time.append(row[0])
	
	if len(time) < 2:
		return 0
	interval = (time[1] - time[0]).seconds
	interval = interval//60
	i = 2
	while interval > 60 and i <= 4:     #keep the time interval less than 1 hour
		interval = (time[i] - time[i-1]).seconds
		interval = interval // 60
		i = i+1
	startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
	endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
	totallength = (endDate - startDate).days
	totallength = totallength *24 * 60     #how many minutes between starttime and endtime
	potential = totallength / interval     #this is the potential record
	actual = 0
	select_stmt = "SELECT COUNT(site_id) FROM continuous_voltage WHERE site_id = %(site_id)s AND " \
				  "time_stamp >= %(sDate)s AND time_stamp < %(eDate)s"
	cursor.execute(select_stmt, {'site_id': siteTmp, 'sDate': startDate, 'eDate': endDate})
	row = cursor.fetchone()
	if row is not None:
		actual = row[0]
	siteCover = actual / potential
	cnn.commit()
	return siteCover


def ylinarxy(x, y, x_i):
	c = y + 0.2649 * x
	return -0.2649 * x_i + c

def xlinarxy(x, y, y_i):
	c = y + 0.2649 * x
	return (y_i - c)/(-0.2649)


#3.3.11.2 Nunber of sags
###not do un-anaualised
def NS(result_set_id, siteTmp):
	###calcluate siteCoverage
	startDate = '2015-07-01'
	endDate = '2016-06-30'
	siteCover = siteCoverage_not_write(startDate, endDate, siteTmp)
	if siteCover == 0:
		return None
	###
	cursor = cnn.cursor()
	select_stmt = "SELECT count(*) FROM discrete_sag_swell WHERE site_id = %(site_id)s and time_stamp >= '2015-07-01 00:00:00' AND time_stamp < '2016-06-30 00:00:00'"      #sag ID here
	cursor.execute(select_stmt, {'site_id': siteTmp})
	result = cursor.fetchone()[0]
	if result == None:
		return None
	cursor.close()

	result = float(result)

	intoData = result/siteCover

	if result_set_id != '!':
		cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
				  "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
		cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': 'NS',
								 'period': 'Annual', 'value': intoData})
		result_cnn.commit()
		cursor.close()


	return result/siteCover     #further opt here


#3.3.11.3 sag index 
def SI(startDate, endDate, siteTmp, result_set_id):
	siteCover = siteCoverage_not_write(startDate, endDate, siteTmp)
	a = NS('!', siteTmp)
	if (a == None or a == 0) and (siteCover == None or siteCover == 0):
		return

	pu = 0
	dur = 0
	dataSet = []
	cursor = cnn.cursor()
	select_stmt = "SELECT time_stamp,voltage_mean_phase_a,voltage_mean_phase_b,voltage_mean_phase_c FROM discrete_sag_swell WHERE site_id = %(site_id)s order by time_stamp"
	cursor.execute(select_stmt, {'site_id': siteTmp})
	a = cursor.fetchone()
	b = cursor.fetchone()
	while a != None and b != None:
		if a[1] != 'NULL' and a[1] is not None:
			pu = dvolt2Pu(float(a[1]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.01 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))
		elif a[2] != 'NULL' and a[2] is not None:
			pu = dvolt2Pu(float(a[2]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.01 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))
		elif a[3] != 'NULL' and a[3] is not None:
			pu = dvolt2Pu(float(a[3]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.01 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))
		a = b
		b = cursor.fetchone()

	if (len(dataSet) == 0):
		return

	i = []
	for data in dataSet:
		if data[0] >= 0.02 and data[0] <= 1 and ylinarxy(0.02, 0.7, data[0]) >= data[1]:
			i.append((np.log10(data[0]) - np.log10(0.02)) / (np.log10(1) - np.log10(0.02)))

		if data[0] >= 0.2 and data[0] <= 25 and ylinarxy(0.5, 0.7, data[0]) < data[1] and ylinarxy(0.5, 0.8, data[0]) >= data[1]:
			i.append((np.log10(data[0]) - np.log10(0.5)) / (np.log10(25) - np.log10(0.5)))

		if data[0] >= 10 and data[0] <= 500 and ylinarxy(10, 0.8, data[0]) < data[1] and ylinarxy(10, 0.9, data[0]) >= data[1]:
			i.append((np.log10(data[0]) - np.log10(10)) / (np.log10(500) - np.log10(10)))

		if data[1] >= 0.25 and data[1] <= 0.7 and xlinarxy(0.02, 0.7, data[1]) >= data[0] and xlinarxy(0.5, 0.7, data[1]) < data[0]:
			i.append((0.7 - data[1]) / (0.7 - 0.25))

		if data[1] >= 0.35 and data[1] <= 0.8 and xlinarxy(0.5, 0.8, data[1]) < data[0] and xlinarxy(10, 0.8, data[1]) >= data[0]:
			i.append((0.8 - data[1]) / (0.8 - 0.55))

		if data[1] >= 0.45 and data[1] <= 0.9 and xlinarxy(10, 0.9, data[1]) < data[0]:
			i.append((0.9 - data[1]) / (0.9 - 0.45))

	print (i)
	if siteCover != 0:
		si = sum(i) / siteCover
		if (endDate - startDate).days > 31:
			period = "Annual"
		else:
			month = startDate.month
			if month == 1:
				period = "January"
			if month == 2:
				period = "February"
			if month == 3:
				period = "March"
			if month == 4:
				period = "April"
			if month == 5:
				period = "May"
			if month == 6:
				period = "June"
			if month == 7:
				period = "July"
			if month == 8:
				period = "August"
			if month == 9:
				period = "September"
			if month == 10:
				period = "October"
			if month == 11:
				period = "November"
			if month == 12:
				period = "December"
			another_cursor = result_cnn.cursor()
			insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
			another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': 'SI',
                                     'period': period, 'value': si})
			result_cnn.commit()
			another_cursor.close()


	cursor.close()



	
#3.3.11.4 Protection index
def PI(result_set_id, siteTmp):
	startDate = '2015-07-01'
	endDate = '2016-06-30'

	siteCover = siteCoverage_not_write(startDate, endDate, siteTmp)
	a = NS('!', siteTmp)
	if (a == None or a == 0) and (siteCover == None or siteCover == 0):
		return

	pu = 0
	dur = 0
	dataSet = []
	cursor = cnn.cursor()
	select_stmt = "SELECT time_stamp,voltage_mean_phase_a,voltage_mean_phase_b,voltage_mean_phase_c FROM discrete_sag_swell WHERE site_id = %(site_id)s order by time_stamp"
	cursor.execute(select_stmt, {'site_id': siteTmp})
	a = cursor.fetchone()
	b = cursor.fetchone()
	#filter the data
	while a != None and b != None:
		if a[1] != 'NULL' and a[1] is not None:
			pu = dvolt2Pu(float(a[1]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.2 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))
		elif a[2] != 'NULL' and a[2] is not None:
			pu = dvolt2Pu(float(a[2]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.2 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))
		elif a[3] != 'NULL' and a[3] is not None:
			pu = dvolt2Pu(float(a[3]), siteTmp)
			dur = (a[0]-b[0]).seconds
			if pu > 0 and pu < 0.9:
				if dur >= 0.2 and dur < 60:
					print (dur, pu)
					dataSet.append((dur, pu))

		a = b
		b = cursor.fetchone()

	if (len(dataSet) == 0):
		return

	sed = []
	for data in dataSet:
		T_p = (data[1] ** 9) * 2.7 + 4.7
		if T_p > data[0]:
			sed.append(0)
		else :
			sed.append(data[0] - T_p)

	sumSED = 0
	if siteCover != 0:
		for s in sed:
			sumSED = sumSED + s ** 2
		pi = np.sqrt((sumSED/len(sed)))

		cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                  "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
		cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': 'PI',
                                 'period': 'Annual', 'value': pi})
		result_cnn.commit()
		cursor.close()
	


#testing function here....
if __name__ == '__main__':
	startDate = '2015-07-01'
	endDate = '2016-06-30'
	NS('l', 'ZA00001')






