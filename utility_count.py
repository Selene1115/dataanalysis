from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *

def utility_count(utilityTmp, result_set_id):  # For LV and MV the total number of sites (with any valid index)
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "site count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()


def utility_AVDI_count(utilityTmp, result_set_id):  # for LV the number of sites with a valid AVDI, only for LV
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'AVDI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]
    index = "AVDI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})
    result_cnn.commit()
    another_cursor.close()


def utility_V99_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid V99
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'V99'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'V99'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "V99 count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()

    index = "V99"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_V1_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid V1
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'V1'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'V1'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "V1 count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "V1"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_VSpread_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid Spread
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'Spread'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'Spread'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "Spread count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "Spread"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_THDI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid THDI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'THDI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'THDI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "THDI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "THDI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_VUFI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid VUFI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'VUFI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'VUFI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "VUFI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "VUFI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)

def utility_HnI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid HnI
    cursor = result_cnn.cursor()
    for n in range(1, 26):
        index = "H" + str(n) + "I"
        select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                      "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                      "AND period = 'Annual' AND index_id = %(index)s"
        cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index': index})
        row = cursor.fetchone()
        LV_count = row[0]

        select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                      "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                      "AND period = 'Annual' AND index_id = %(index)s"
        cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index': index})
        row = cursor.fetchone()
        MV_count = row[0]

        index = index + " count"
        period = "Annual"
        statistic = "LV count"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(LV_count)})

        statistic = "MV count"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(MV_count)})
        result_cnn.commit()
        another_cursor.close()
        index = "H" + str(n) + "I"
        #count the number of sites based on different nominal voltage
        nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
        nominal_voltage11_count(utilityTmp, result_set_id, index)
        nominal_voltage_count(utilityTmp, result_set_id, index, 33)
        nominal_voltage_count(utilityTmp, result_set_id, index, 66)
        nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_HCI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid HCI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'VUFI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'VUFI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "HCI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "HCI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_PSTI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid PSTI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'PSTI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'PSTI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "PSTI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()

    index = "PSTI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_PLTI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid PLTI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'PLTI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'PLTI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "PLTI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "PLTI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


def utility_SI_count(utilityTmp, result_set_id):  # for LV and MV the number of sites with a valid SI
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = 'SI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    LV_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = 'SI'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp})
    row = cursor.fetchone()
    MV_count = row[0]

    index = "SI count"
    period = "Annual"
    statistic = "LV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "SI"
    #count the number of sites based on different nominal voltage
    nominal_voltage_count(utilityTmp, result_set_id, index, 6.6)
    nominal_voltage11_count(utilityTmp, result_set_id, index)
    nominal_voltage_count(utilityTmp, result_set_id, index, 33)
    nominal_voltage_count(utilityTmp, result_set_id, index, 66)
    nominal_voltage_count(utilityTmp, result_set_id, index, 132)


#For MV sites the number of sites in the following nominal voltage categories SHALL also be calculated: 6.6 kV,
# 11/22 kV, 33 kV, 66 kV, 133 kV
#count the number of valid index value
def nominal_voltage_count(utilityTmp, result_set_id, index, nominal):   #not for 11/22 kV
    cursor = result_cnn.cursor()
    nominal_voltage = nominal * 1000
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'" \
                  "AND nominal_voltage = %(nominal_voltage)s)" \
                  "AND period = 'Annual' AND index_id = %(index)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp,
                                 'nominal_voltage': nominal_voltage, 'index': index})
    row = cursor.fetchone()
    nominal_voltage_count = row[0]

    index = index + " count"
    period = "Annual"
    statistic = "MV count " + str(nominal)
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(nominal_voltage_count)})
    result_cnn.commit()
    another_cursor.close()


def nominal_voltage11_count(utilityTmp, result_set_id, index):  # for 11/22 kV
    cursor = result_cnn.cursor()
    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'" \
                  "AND nominal_voltage = '11OOO') AND period = 'Annual' AND index_id = %(index)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index': index})
    row = cursor.fetchone()
    nominal_voltage_count = row[0]

    select_stmt = "SELECT COUNT(site_id) FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'" \
                  "AND nominal_voltage = '22OOO') AND period = 'Annual' AND index_id = %(index)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index': index})
    row = cursor.fetchone()
    nominal_voltage_count = nominal_voltage_count + row[0]

    index = index + " count"
    period = "Annual"
    statistic = "MV count 11/22kV"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(nominal_voltage_count)})
    result_cnn.commit()
    another_cursor.close()