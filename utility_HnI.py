from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *
from index_statistics import *

def utility_HnI(utilityTmp, result_set_id):
    for n in range(1, 26):
        index = "H" + str(n) + "I"
        index_statistics(utilityTmp, "Annual", index, result_set_id)  #call index_statistics function from H1I to H25I