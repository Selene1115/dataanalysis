from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *

def PCL_AVDI(siteTmp, result_set_id):    #only for LV site 
    AVDIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "AVDI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'AVDI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        AVDIndex = float(row[0])
    AVDI_PCL = 0
    if AVDIndex != "NULL":   #if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                 'voltage_level': level})  # limit for AVDI
            row = cursor.fetchall()
            if len(row) == 0:  # get default limit
                default_juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index,
                                     'voltage_level': level})
                row = cursor.fetchall()
                for r in row:
                    default_limit = r[0]
                AVDIlimit = float(default_limit)
            else:
                for r in row:
                    AVDIlimit = float(r[0])
            AVDI_PCL = AVDIndex / AVDIlimit
            period = "Annual"
            index = "Site AVDI PCL"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                          "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                                 'period': period, 'value': float(AVDI_PCL)})
            result_cnn.commit()
            another_cursor.close()
    return AVDI_PCL

def PCL_CSI(siteTmp, result_set_id):   
    CSIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "CSI_ndist"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'CSI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        CSIndex = float(row[0])
    CSI_PCL = 0
    if CSIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for CSI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            CSIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for CSI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        CSIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        CSIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    CSIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                CSIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        CSI_PCL = CSIndex / CSIlimit
        period = "Annual"
        index = "Site CSI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(CSI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return CSI_PCL

def PCL_HnI(siteTmp, n, result_set_id):
    HnIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "H" + str(n) + "I"
    select_stmt = "SELECT value FROM site_result WHERE index_id = %(index_id)s AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'index_id':index, 'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        HnIndex = float(row[0])
    HnI_PCL = 0
    if HnIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for HnI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            HnIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for HnI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        HnIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        HnIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    HnIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                HnIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        HnI_PCL = HnIndex / HnIlimit
        period = "Annual"
        index = "Site H" + str(n) + "I PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(HnI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return HnI_PCL

def site_PCL_HnI(siteTmp, result_set_id):   #call PCL_HnI function
    for n in range(1, 26):
        PCL_HnI(siteTmp, n, result_set_id)

def PCL_HCI(siteTmp, result_set_id):
    HCIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "HCI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'HCI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        HCIndex = float(row[0])
    HCI_PCL = 0
    if HCIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for HCI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            HCIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for HCI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        HCIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        HCIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    HCIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                HCIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        HCI_PCL = HCIndex / HCIlimit
        period = "Annual"
        index = "Site HCI_PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(HCI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return HCI_PCL

def PCL_PSTI(siteTmp, result_set_id):
    PSTIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "PSTI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'PSTI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        PSTIndex = float(row[0])
    PSTI_PCL = 0
    if PSTIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for PSTI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            PSTIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for PSTI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        PSTIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        PSTIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    PSTIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                PSTIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal volta
        PSTI_PCL = PSTIndex / PSTIlimit
        period = "Annual"
        index = "Site PSTI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(PSTI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return PSTI_PCL

def PCL_PLTI(siteTmp, result_set_id):
    PLTIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "PLTI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'PLTI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        PLTIndex = float(row[0])
    PLTI_PCL = 0
    if PLTIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for PLTI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            PLTIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for PLTI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        PLTIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        PLTIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    PLTIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                PLTIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        PLTI_PCL = PLTIndex / PLTIlimit
        period = "Annual"
        index = "Site PLTI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(PLTI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return PLTI_PCL

def PCL_SI(siteTmp, result_set_id):
    SIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "SI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'SI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        SIndex = float(row[0])
    SI_PCL = 0
    if SIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for SI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            SIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for SI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        SIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        SIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    SIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                SIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        SI_PCL = SIndex / SIlimit
        period = "Annual"
        index = "Site SI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(SI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return SI_PCL

def PCL_SPREAD(siteTmp, result_set_id):
    vSpread = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "Spread"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'Spread' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        vSpread = float(row[0])
    SPREAD_PCL = 0
    if vSpread != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for Spread
            row = cursor.fetchone()
            if row is None:  # get default limit
                default_juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            Spreadlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for Spread
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        Spreadlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        Spreadlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    Spreadlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                Spreadlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        SPREAD_PCL = vSpread / Spreadlimit
        period = "Annual"
        index = "Site Spread PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(SPREAD_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return SPREAD_PCL

def PCL_THDI(siteTmp, result_set_id):
    THDIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "THDI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'THDI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        THDIndex = float(row[0])
    THDI_PCL = 0
    if THDIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for THDI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            THDIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for THDI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        THDIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        THDIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    THDIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                THDIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        THDI_PCL = THDIndex / THDIlimit
        period = "Annual"
        index = "Site THDI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(THDI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return THDI_PCL

def PCL_VUFI(siteTmp, result_set_id):
    VUFIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]  # for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    index = "VUFI"
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'VUFI' AND result_set_id = %(result_set_id)s " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        VUFIndex = float(row[0])
    VUFI_PCL = 0
    if VUFIndex != "NULL":  # if we can get valid index
        if level == "LV":
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'voltage_level': level})  # limit for VUFI
            row = cursor.fetchone()
            if row is None:  # get default limit
                juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchone()
            VUFIlimit = float(row[0])
        if level == "MV":
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for VUFI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        VUFIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        VUFIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    VUFIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                VUFIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        VUFI_PCL = VUFIndex / VUFIlimit
        period = "Annual"
        index = "Site VUFI PCL"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(VUFI_PCL)})
        result_cnn.commit()
        another_cursor.close()
    return VUFI_PCL