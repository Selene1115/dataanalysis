#This is the main interface of the data analysis
#all function is called from this function


from database_connections import *
import sys
import PyQt5
import numpy as np
from PyQt5 import QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtQml import *
from PyQt5 import uic
from PyQt5.QtCore import *
from coverage_filter import *
from filter import *
from normalization import *
from sag import *
from site_histogram import *
from site_indices import *
from site_PCL import *
from utility_HnI import *
from ut_class import *
from index_statistics import *
from database_connections import *
from median_site_change import *
from utility_count_over_limit import *
from utility_count import *
import time
from coverage_limit import *
from coverage_valid import *
from coverage_filter import *

import mysql.connector
from mysql.connector import errorcode
import re
__author__ = {'name' : 'Changze Huang'}

#buffer the utility and site id
allUtility = []
allName = []
allSite = []
nameTable = []
internet = False

#load utility and stie data here, also check the internet
while internet == False:
	if connecttion() != False:
		internet = True
		cursor = cnn.cursor()
		select_stmt = "SELECT utility_id, utility_name FROM utility"
		cursor.execute(select_stmt)

		row = cursor.fetchone()
		while row != None:
			allUtility.append(row[0])
			allName.append(row[1])
			nameTable.append((row[0], row[1]))
			row = cursor.fetchone()
	else:
		internet = False
	print (allUtility)

	if connecttion() != False:
		for ut in allUtility:
			cursor = cnn.cursor()
			select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility_id)s"
			cursor.execute(select_stmt, {'utility_id': str(ut)})
			row = cursor.fetchone()
			while row is not None:
				allSite.append((ut, row[0]))
				row = cursor.fetchone()
	else:
		internet = False
	
	if connecttion() != False:
		cursor = result_cnn.cursor()
		resultSet = []
		select_stmt = "SELECT DISTINCT result_set_id FROM utility_result where result_set_id = '2015-2016 Full Run'"
		cursor.execute(select_stmt)
		row = cursor.fetchone()
		while row != None:
			resultSet.append(row[0])
			row = cursor.fetchone()	
	else:
		internet = False
	
	if internet == False:
		str = input("No connectiton, 'r' to try again other input to quit:")
		if str == 'r':
			continue
		else:
			sys.exit()
	
qtCreatorFile = "it3.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
	PyQt5.QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

if hasattr(QtCore.Qt, 'AA_UseHighDpiPixmaps'):
	PyQt5.QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)

#the thread for calulation
class Thread(QThread):
		sig = QtCore.pyqtSignal(int)

		def __init__(self, startDate, endDate, IDEidt, siteDo, siteTree, utDo, utTree, progressBar, workLoad, MSC, msc1):
			QThread.__init__(self)
			self.startDate = startDate
			self.endDate = endDate
			self.IDEidt = IDEidt
			self.siteDo = siteDo
			self.siteTree = siteTree
			self.utDo = utDo
			self.utTree = utTree
			self.progressBar = progressBar
			self.workLoad = workLoad
			self.MSC = MSC
			self.msc1 = msc1
			self.sig.connect(self.updateProgBar)

		def __del__(self):
			self.wait()
		
		def updateProgBar(self, i):
			self.progressBar.setValue(self.progressBar.value()+i)

		def run(self):
			mn = ['Annual', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'December']
			indexListUt = ['V99', 'V1', 'VSpread', 'AVDI', 'VUFI', 'THDI', 'HCI', 'PSTI', 'PLTI', 'SI']
			feeder = ['CBD', 'Urban', 'Short rural', 'Long rural']
			load = ['Residential', 'Commercial', 'Industrial', 'Mixed']
			hxi = []
			for a in range(25):
				hxi.append("H" + str(a + 1) + "I")

		####get date
			a = self.startDate.date()
			startTime = ''

			startTime = str(a.year())
			if a.month() < 10:
				startTime = startTime + '-0' + str(a.month())
			else:
				startTime = startTime + '-' + str(a.month())
			
			if a.day() < 10:
				startTime = startTime + '-0' + str(a.day())
			else:
				startTime = startTime + '-' + str(a.day())
			

			a = self.endDate.date()
			endTime = ''

			endTime = str(a.year())
			if a.month() < 10:
				endTime = endTime + '-0' + str(a.month())
			else:
				endTime = endTime + '-' + str(a.month())
			
			if a.day() < 10:
				endTime = endTime + '-0' + str(a.day())
			else:
				endTime = endTime + '-' + str(a.day())

		####get id
			result_set_id = self.IDEidt.text()
			print ("here2")

		#####do site
			for zi in range(self.siteDo.count()):
				siteTmp = self.siteDo.item(zi).text()
				for i in range(self.siteTree.topLevelItemCount()):
					if self.siteTree.topLevelItem(i).checkState(0):
						do_index = self.siteTree.topLevelItem(i).text(0)

						if do_index == 'AVDI':
							self.sig.emit(1)
							siteAVDI(siteTmp, startTime, endTime, result_set_id)
						if do_index == 'Spread:':
							VSpread(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)
						elif do_index == 'V1':
							V1(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)

						elif do_index == 'V99':
							V99(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)

						elif do_index == 'VUFI':
							VUFI(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)
						elif do_index == 'THDI':
							THDI(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)
						elif do_index == 'HII':
							HII(siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'HnI':
							site_HnI(siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'HCI':
							HCI(siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'PSTI':
							PSTI(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)
						elif do_index == 'PLTI':
							PLTI(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)
						elif do_index == 'CSI':
							CSI(siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'NS':
							NS(result_set_id, siteTmp)
							self.sig.emit(1)
						elif do_index == 'SI':
							SI(startTime, endTime, siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'PI':
							PI(result_set_id, siteTmp)
							self.sig.emit(1)
						elif do_index == 'PCL':
							PCL_AVDI(siteTmp, result_set_id)    
							self.sig.emit(1)
							PCL_CSI(siteTmp, result_set_id)
							self.sig.emit(1)
							site_PCL_HnI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_HCI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_PSTI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_PLTI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_SI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_SPREAD(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_THDI(siteTmp, result_set_id)
							self.sig.emit(1)

							PCL_VUFI(siteTmp, result_set_id)
							self.sig.emit(1)


						elif do_index == 'Histogram':
							histogram_PST(siteTmp, result_set_id)
							self.sig.emit(1)

							histogram_PLT(siteTmp, result_set_id)
							self.sig.emit(1)

							histogram_THD(siteTmp, result_set_id)
							self.sig.emit(1)

							histogram_voltage(siteTmp, result_set_id)
							self.sig.emit(1)

							histogram_unbalance(siteTmp, result_set_id)
							self.sig.emit(1)
						elif do_index == 'Site_Coverage':
							siteCoverage(siteTmp, startTime, endTime, result_set_id)
							self.sig.emit(1)


		###do ut	
		
			for zi in range(self.utDo.count()):
				utTmp = self.utDo.item(zi).text()
				for i in range(self.utTree.topLevelItemCount()):
					if self.utTree.topLevelItem(i).checkState(0) == Qt.Checked or self.utTree.topLevelItem(i).checkState(0) == Qt.PartiallyChecked:
						index = self.utTree.topLevelItem(i).text(0)
						selectedItem = self.utTree.topLevelItem(i)
						if index == 'Index_Statistics':
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									for m in mn:
										if selectedItem.child(j).text(0) == 'HnI':
											utility_HnI(utTmp, result_set_id)
											self.sig.emit(1)
										else:
											index_statistics(utTmp, m, selectedItem.child(j).text(0), result_set_id)
											self.sig.emit(1)
								
						elif index == 'Non-index_Statistics':
						#####change later, wait to be one function
							utility_count(utTmp, result_set_id)
							self.sig.emit(1)
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									print (selectedItem.child(j).text(0))
									if selectedItem.child(j).text(0) == 'HnI':
										for k in range(2,26):
											iL = "H" + str(k) + "I"
											coverage_limit(utTmp, iL, result_set_id)
											self.sig.emit(1)
											coverage_valid(utTmp, iL, result_set_id)
											self.sig.emit(1)
									else:
										coverage_limit(utTmp, selectedItem.child(j).text(0), result_set_id)
										self.sig.emit(1)
										coverage_valid(utTmp, selectedItem.child(j).text(0), result_set_id)
										self.sig.emit(1)
									
									if selectedItem.child(j).text(0) == 'AVDI':
										utility_AVDI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_AVDI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'V1':
										utility_V1_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_V1_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'V99':
										utility_V99_count(utTmp, result_set_id)
										self.sig.emit(1)
										utility_V99_limit(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'Spread':
										utility_VSpread_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_VSpread_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'VUFI':
									
										utility_VUFI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_VUFI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'THDI':
										utility_THDI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_THDI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'PLTI':
										utility_PLTI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_PLTI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'PSTI':
							
										utility_PSTI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_PSTI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'SI':
										utility_SI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_SI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'HCI':
							
										utility_HCI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_HCI_count(utTmp, result_set_id)
										self.sig.emit(1)
									elif selectedItem.child(j).text(0) == 'HnI':
										utility_HnI_limit(utTmp, result_set_id)
										self.sig.emit(1)
										utility_HnI_count(utTmp, result_set_id)
										self.sig.emit(1)
						elif index == 'Index_Statistics_By_Class':
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									iL = selectedItem.child(j).text(0)
									for m in mn:
										utility_class(utTmp, iL, 'Strong', 'MV', m, 95, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Strong', 'LV', m, 95, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Weak', 'MV', m, 95, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Weak', 'LV', m, 95, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Weak', 'LV', m, 50, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Weak', 'MV', m, 50, result_set_id)
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Strong', 'LV', m, 50, result_set_id)											
										self.sig.emit(1)
										utility_class(utTmp, iL, 'Strong', 'MV', m, 50, result_set_id)
										self.sig.emit(1)

										for fe in feeder:   
											utility_class_f(utTmp, iL, 'Strong', 'MV', fe, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Strong', 'LV', fe, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Weak', 'MV', fe, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Weak', 'LV', fe, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Weak', 'LV', fe, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Weak', 'MV', fe, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Strong', 'LV', fe, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_f(utTmp, iL, 'Strong', 'MV', fe, m, 50, result_set_id)
											self.sig.emit(1)
	
										for lo in load:
											utility_class_l(utTmp, iL, 'Strong', 'MV', lo, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Strong', 'LV', lo, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Weak', 'MV', lo, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Weak', 'LV', lo, m, 95, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Weak', 'LV', lo, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Weak', 'MV', lo, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Strong', 'LV', lo, m, 50, result_set_id)
											self.sig.emit(1)
											utility_class_l(utTmp, iL, 'Strong', 'MV', lo, m, 50, result_set_id)
											self.sig.emit(1)
						elif index == 'PCL':
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									iL = selectedItem.child(j).text(0)
									for m in mn:
										PCL_utility(utTmp, iL, m, result_set_id)
										self.sig.emit(1)
						elif index == 'Performance_By_Class':
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									iL = selectedItem.child(j).text(0)
									for m in mn:
										perf_utility_class(utTmp, iL, 'Strong', 'MV', result_set_id)
										self.sig.emit(1)
										perf_utility_class(utTmp, iL, 'Weak', 'MV', result_set_id)
										self.sig.emit(1)
										perf_utility_class(utTmp, iL, 'Strong', 'LV', result_set_id)
										self.sig.emit(1)
										perf_utility_class(utTmp, iL, 'Weak', 'LV', result_set_id)
										self.sig.emit(1)
						elif index == 'PNV':
							for j in range(selectedItem.childCount()):
								if selectedItem.child(j).checkState(0) == Qt.Checked:
									iL = selectedItem.child(j).text(0)
									for m in mn:
										predicted_network(utTmp, iL, result_set_id)
										self.sig.emit(1)
			
				if self.MSC.checkState == Qt.Checked:
					print ("aaa1")
					proceding_result = self.msc1.currentText()
					for iL in indexListUt:
						median_site_change(result_set_id, proceding_result, iL, utTmp)
			
#The main window class							
class MyApp(QMainWindow, Ui_MainWindow):
	def __init__(self):
		QMainWindow.__init__(self)
		Ui_MainWindow.__init__(self)
		# self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
		# self.setFixedSize(self.size())
		self.setupUi(self)
		for ut in nameTable:
			item = QListWidgetItem(ut[0])
			self.utNot.addItem(item)
			nameItem = QListWidgetItem(ut[1])
			self.nameNot.addItem(nameItem)
		self.nameNot.sortItems()
		
		for set in resultSet:
			self.msc1.addItem(set)
		
		# for site in allSite:
			# self.addItem('siteNot', site[1])

			
		self.utDo.hide()
		self.utNot.hide()
		self.workLoad = 0
		self.setWindowTitle("PQA Data Analysis")
		
		self.utTree.setToolTip("If you do not select any utility\nThis section will grey out")
		self.siteTree.setToolTip("If you do not select any site\nThis section will grey out")
		self.nameDo.setToolTip("Calulation will include utility in this list")
		self.nameNot.setToolTip("Calulation will not include utility in this list")
		self.siteNot.setToolTip("Calulation will not include site in this list")
		self.siteDo.setToolTip("Calulation will include site in this list")
		
		self.utNot.setSelectionMode(QAbstractItemView.MultiSelection)
		self.utDo.setSelectionMode(QAbstractItemView.MultiSelection)
		self.nameDo.setSelectionMode(QAbstractItemView.MultiSelection)
		self.nameNot.setSelectionMode(QAbstractItemView.MultiSelection)
		self.siteNot.setSelectionMode(QAbstractItemView.MultiSelection)
		self.siteDo.setSelectionMode(QAbstractItemView.MultiSelection)

		self.searchUt.setToolTip("You can search utility here")
		self.searchSite.setToolTip("You can search site here")
		
		self.searchUt.textChanged.connect(self.SearchUt)
		self.searchSite.textChanged.connect(self.SearchSite)
		self.IDEidt.textChanged.connect(self.ifListFill)
		
		myDate='2015-07-01'
		qtDateStart = QDate.fromString(myDate, 'yyyy-MM-dd')
		myDate='2016-07-01'
		qtDateEnd = QDate.fromString(myDate, 'yyyy-MM-dd')

		self.startDate.setDate(qtDateStart)
		self.endDate.setDate(qtDateEnd)
		self.startDate.setCalendarPopup(True)
		self.endDate.setCalendarPopup(True)
		self.startDate.dateChanged.connect(self.ifListFill)
		self.endDate.dateChanged.connect(self.ifListFill)

		
		self.moveOneUt.setToolTip("move selected utility to calulation list")
		self.moveAllUt.setToolTip("move all utility to calulation list")
		self.removeOneUt.setToolTip("remove selected utility to calulation list")
		self.removeAllUt.setToolTip("remove all utility to calulation list")
		
		self.moveOneUt.clicked.connect(self.moveUtItem)
		self.moveAllUt.clicked.connect(self.moveAllUT)
		self.removeOneUt.clicked.connect(self.removeUtItem)
		self.removeAllUt.clicked.connect(self.removeAllUT)
		
		
		self.moveOneSite.setToolTip("move selected site to calulation list")
		self.moveAllSite.setToolTip("move all site to calulation list")
		self.removeOneSite.setToolTip("remove selected site to calulation list")
		self.removeAllSite.setToolTip("remove all site to calulation list")
		
		self.moveOneSite.clicked.connect(self.moveSiteItem)
		self.moveAllSite.clicked.connect(self.moveAllSIte)
		self.removeOneSite.clicked.connect(self.removeSiteItem)
		self.removeAllSite.clicked.connect(self.removeAllSIte)
		self.goButtom.clicked.connect(self.doThings)
		self.clean_all.clicked.connect(self.reSet)
		self.actionExit.triggered.connect(qApp.quit)

		self.setCursor(QCursor(QtCore.Qt.ArrowCursor))

		self.goButtom.setStyleSheet("QPushButton:disabled { background-color: grey}")
		
		
		self.nameNot.itemDoubleClicked.connect(self.handleName)
		self.nameDo.itemDoubleClicked.connect(self.handleNameDO)
		self.siteNot.itemDoubleClicked.connect(self.handleSite)
		self.siteDo.itemDoubleClicked.connect(self.handleSiteDo)

		
		self.goButtom.setEnabled(False)
		self.ifListFill()
		self.progressBar.setValue(0)


	def handleSiteDo(self):
		for i in range(self.siteDo.count()):
			if self.siteDo.currentItem() == self.siteDo.item(i):
				self.siteDo.item(i).setSelected(True)
			else:
				self.siteDo.item(i).setSelected(False)

		self.removeSiteItem()	
		
		
	def handleSite(self):
		for i in range(self.siteNot.count()):
			if self.siteNot.currentItem() == self.siteNot.item(i):
				self.siteNot.item(i).setSelected(True)
			else:
				self.siteNot.item(i).setSelected(False)

		self.moveSiteItem()	
		
		
	def handleNameDO(self):
		for i in range(self.nameDo.count()):
			if self.nameDo.currentItem() == self.nameDo.item(i):
				self.nameDo.item(i).setSelected(True)
			else:
				self.nameDo.item(i).setSelected(False)

		self.removeUtItem()
		
	def handleName(self):
		for i in range(self.nameNot.count()):
			if self.nameNot.currentItem() == self.nameNot.item(i):
				self.nameNot.item(i).setSelected(True)
			else:
				self.nameNot.item(i).setSelected(False)

		self.moveUtItem()
	
	
	def ifListFill(self):
		
		if self.siteDo.count() == 0:
			for i in range(self.siteTree.topLevelItemCount()):
				self.siteTree.topLevelItem(i).setDisabled(True)
				self.siteTree.setCursor(QCursor(QtCore.Qt.ForbiddenCursor))
		elif self.siteDo.count() != 0:
			self.siteTree.setCursor(QCursor(QtCore.Qt.ArrowCursor))
			for i in range(self.siteTree.topLevelItemCount()):
				self.siteTree.topLevelItem(i).setDisabled(False)
			
		if self.utDo.count() == 0:
			for i in range(self.utTree.topLevelItemCount()):
				self.utTree.setCursor(QCursor(QtCore.Qt.ForbiddenCursor))
				self.utTree.topLevelItem(i).setDisabled(True)
		elif self.utDo.count() != 0:
			self.utTree.setCursor(QCursor(QtCore.Qt.ArrowCursor))
			for i in range(self.utTree.topLevelItemCount()):
				self.utTree.topLevelItem(i).setDisabled(False)
		
		if self.startDate.date() >= self.endDate.date():
			self.goButtom.setEnabled(False)
			return
		
		if self.siteDo.count() == 0 and self.utDo.count() == 0:
			self.goButtom.setEnabled(False)
			return
		elif self.siteDo.count() != 0 or self.utDo.count() != 0:
			self.goButtom.setEnabled(True)
		
		if self.IDEidt.text() == '':
			self.goButtom.setEnabled(False)
			return
			
		
	def getWorkLoad(self):
		i = 0
		mn = ['Annual', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'December']
		indexListUt = ['V99', 'V1', 'VSpread', 'AVDI', 'VUFI', 'THDI', 'HCI', 'PSTI', 'PLTI', 'SI']
		feeder = ['CBD', 'Urban', 'Short rural', 'Long rural']
		load = ['Residential', 'Commercial', 'Industrial', 'Mixed']
		hxi = []
		for a in range(25):
			hxi.append("H" + str(a + 1) + "I")

		for zi in range(self.siteDo.count()):
			siteTmp = self.siteDo.item(zi).text()
			for k in range(self.siteTree.topLevelItemCount()):
				if self.siteTree.topLevelItem(k).checkState(0) == Qt.Checked:
					do_index = self.siteTree.topLevelItem(k).text(0)
					if do_index == 'AVDI':
						i  = i + 1
					elif do_index == 'Spread':
						i = i + 1
					elif do_index == 'V1':
						i = i + 1
					elif do_index == 'V99':
						i = i + 1
					elif do_index == 'VUFI':
						i = i + 1
					elif do_index == 'THDI':
						i = i + 1
					elif do_index == 'HII':
						i = i + 1
					elif do_index == 'HnI':
						i = i + 1
					elif do_index == 'HCI':
						i = i + 1
					elif do_index == 'PSTI':
						i = i + 1
					elif do_index == 'PLTI':
						i= i + 1

					elif do_index == 'CSI':
						i= i + 1
					elif do_index == 'NS':
						i= i + 1
					elif do_index == 'SI':
						i= i + 1
					elif do_index == 'PI':
						i= i + 1
					elif do_index == 'PCL':
						i = i + 10
					elif do_index == 'Histogram':
						i = i + 5
					elif do_index == 'Site_Coverage':
						i = i + 1
				
		for zi in range(self.utDo.count()):
			utTmp = self.utDo.item(zi).text()
			for k in range(self.utTree.topLevelItemCount()):
				if self.utTree.topLevelItem(k).checkState(0) == Qt.Checked or self.utTree.topLevelItem(k).checkState(0) == Qt.PartiallyChecked:
					index = self.utTree.topLevelItem(k).text(0)
					selectedItem = self.utTree.topLevelItem(k)
					if index == 'Index_Statistics':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								for m in mn:
									i = i + 1
					elif index == 'Non-index_Statistics':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								if selectedItem.child(j).text(0) == 'HnI':
									for k in range(2,26):
										iL = "H" + str(k) + "I"
										i = i + 2
								else:
									i =i + 2
									
								if selectedItem.child(j).text(0) == 'AVDI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'V1':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'V99':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'Spread':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'VUFI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'THDI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'PLTI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'PSTI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'SI':
									i =i + 2
								elif selectedItem.child(j).text(0) == 'HCI':
							
									i =i + 2
								elif selectedItem.child(j).text(0) == 'HnI':
									i =i + 2
					elif index == 'Index_Statistics_By_Class':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								iL = selectedItem.child(j).text(0)
								for m in mn:
									i = i + 8
									for fe in feeder:   
										i = i + 8
									for lo in load:
										i = i + 8
					elif index == 'PCL':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								iL = selectedItem.child(j).text(0)
								for m in mn:
									i = i + 1
					elif index == 'Performance_By_Class':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								iL = selectedItem.child(j).text(0)
								for m in mn:
									i = i + 4
					elif index == 'PNV':
						for j in range(selectedItem.childCount()):
							if selectedItem.child(j).checkState(0) == Qt.Checked:
								iL = selectedItem.child(j).text(0)
								for m in mn:
									i = i + 1

			if self.MSC.checkState == Qt.Checked:
				for iL in indexListUt:
					i = i + 1
		return i

	
	def doThings(self):
		self.goButtom.setEnabled(False)
		self.clean_all.setEnabled(False)
		workLoad = self.getWorkLoad()
		print (workLoad)
		self.workLoad = 0
		self.progressBar.setMaximum(workLoad)
		self.progressBar.setValue(0)

		self.myThread = Thread(self.startDate, self.endDate, self.IDEidt, self.siteDo, self.siteTree, self.utDo, self.utTree, self.progressBar, workLoad, self.MSC, self.msc1)

		self.myThread.finished.connect(self.doClean)
		self.myThread.start()
		
	
		
	def SearchUt(self, string):
		tmpStr = string.upper()
		tmpStr.strip('\r')

		flag = 0
		side = 'a'
		
		for i in range(self.nameNot.count()):
			self.nameNot.item(i).setSelected(False)
		for i in range(self.nameDo.count()):
			self.nameDo.item(i).setSelected(False)
		
		if not tmpStr:
			for i in range(self.nameNot.count()):
				self.nameNot.item(i).setSelected(False)
				self.nameNot.item(i).setHidden(False)
			for i in range(self.nameDo.count()):
				self.nameDo.item(i).setSelected(False)
				self.nameDo.item(i).setHidden(False)
		else:
			for i in range(self.nameNot.count()):
				if re.search(tmpStr, self.nameNot.item(i).text(), re.M|re.I):
					self.nameNot.item(i).setHidden(False)
					flag = flag + 1
					mark = i
					side = 'not'
				else:
					self.nameNot.item(i).setHidden(True)
			for i in range(self.nameDo.count()):
				if re.search(tmpStr, self.nameDo.item(i).text(), re.M|re.I):
					self.nameDo.item(i).setHidden(False)
					flag = flag + 1
					mark = i
					side = 'do'
				else:
					self.nameDo.item(i).setHidden(True)
			
			if flag == 1:
				if side == 'not':
					self.nameNot.item(mark).setSelected(True)
				elif side == 'do':
					self.nameDo.item(mark).setSelected(True)
		
	def SearchSite(self, string):
		tmpStr = string.upper()
		tmpStr.strip('\r')
		
		flag = 0
		side = 'a'
		
		for i in range(self.siteNot.count()):
			self.siteNot.item(i).setSelected(False)
		for i in range(self.siteDo.count()):
			self.siteDo.item(i).setSelected(False)

		
		if not tmpStr:
			for i in range(self.siteNot.count()):
				self.siteNot.item(i).setHidden(False)
			for i in range(self.siteDo.count()):
				self.siteDo.item(i).setHidden(False)
		else:
			for i in range(self.siteNot.count()):
				if re.search(tmpStr, self.siteNot.item(i).text(), re.M|re.I):
					self.siteNot.item(i).setHidden(False)
					flag = flag + 1
					mark = i
					side = 'not'
				else:
					self.siteNot.item(i).setHidden(True)
			for i in range(self.siteDo.count()):
				if re.search(tmpStr, self.siteDo.item(i).text(), re.M|re.I):
					self.siteDo.item(i).setHidden(False)
					flag = flag + 1
					mark = i
					side = 'do'
				else:
					self.siteDo.item(i).setHidden(True)
					
			if flag == 1:
				if side == 'not':
					self.siteNot.item(mark).setSelected(True)
				elif side == 'do':
					self.siteDo.item(mark).setSelected(True)

	def addItem(self, listName, name):
		if listName == 'utNot':
			self.utNot.addItem(name)
		else:
			self.siteNot.addItem(str(name))

			
	def reSet(self):
		self.progressBar.setValue(0)
		
		self.clean_all.setEnabled(True)
		self.searchSite.clear()
		self.searchUt.clear()
		for i in range(self.utDo.count()):
			self.utNot.addItem(self.utDo.takeItem(0))
		
		for i in range(self.siteDo.count()):
			self.siteNot.addItem(self.siteDo.takeItem(0))
			
		for i in range(self.nameDo.count()):
			self.nameNot.addItem(self.nameDo.takeItem(0))
		self.siteNot.clear()
		self.IDEidt.clear()
		
		self.utNot.sortItems()
		self.ifListFill()


	def doClean(self):
		self.progressBar.setValue(0)
		QMessageBox.information(self, "Message", "All calulation is finished")
		
		self.clean_all.setEnabled(True)
		self.searchSite.clear()
		self.searchUt.clear()
		for i in range(self.utDo.count()):
			self.utNot.addItem(self.utDo.takeItem(0))
		
		for i in range(self.siteDo.count()):
			self.siteNot.addItem(self.siteDo.takeItem(0))
			
		for i in range(self.nameDo.count()):
			self.nameNot.addItem(self.nameDo.takeItem(0))
		self.siteNot.clear()
		self.IDEidt.clear()
		
		self.utNot.sortItems()
		self.ifListFill()
	
	def doSiteCheck(self):
		self.siteNot.clear()
		for i in range(self.utDo.count()):
			for ss in allSite:
				if ss[0] == self.utDo.item(i).text():
					self.addItem('siteNot', ss[1])
			
###move function			
	def moveUtItem(self):
		
		items = self.nameNot.selectedItems()
		for selectedItem in items:
			self.nameDo.addItem(self.nameNot.takeItem(self.nameNot.row(selectedItem)))
			for name in nameTable:
				if name[1] == selectedItem.text():
					res = self.utNot.findItems(name[0], Qt.MatchExactly)
					res[0].setSelected(True)
		
		it = self.utNot.selectedItems()		
		if len(it) == 0:
			return

		for i in it:
			self.utDo.addItem(self.utNot.takeItem(self.utNot.row(i)))
		self.searchUt.clear()
		self.doSiteCheck()
		self.ifListFill()
		self.utNot.sortItems()
		self.utDo.sortItems()
		self.nameNot.sortItems()
		self.nameDo.sortItems()
		
	def moveSiteItem(self):

		it = self.siteNot.selectedItems()
		if len(it) == 0:
			return

		for i in it:
			self.siteDo.addItem(self.siteNot.takeItem(self.siteNot.row(i)))
		self.searchSite.clear()
		self.ifListFill()
		self.siteNot.sortItems()
		self.siteDo.sortItems()
###all move function		
	def moveAllUT(self):
		for i in range(self.utNot.count()):
			for ss in allSite:
				if ss[0] == self.utNot.item(0).text():
					self.addItem('siteNot', ss[1])
			
			self.utDo.addItem(self.utNot.takeItem(0))
			self.nameDo.addItem(self.nameNot.takeItem(0))

			
		self.ifListFill()
		self.nameNot.sortItems()
		self.nameDo.sortItems()
		
	def moveAllSIte(self):
		for i in range(self.siteNot.count()):
			self.siteDo.addItem(self.siteNot.takeItem(0))
		self.ifListFill()
		self.siteNot.sortItems()
		self.siteDo.sortItems()

###remove all function
	def removeUtItem(self):
	
		item = self.nameDo.selectedItems()
		for selectedItem in item:
			self.nameNot.addItem(self.nameDo.takeItem(self.nameDo.row(selectedItem)))
			for name in nameTable:
				if name[1] == selectedItem.text():
					res = self.utDo.findItems(name[0], Qt.MatchExactly)
					res[0].setSelected(True)
	
		Items = self.utDo.selectedItems()
		removeSet = []		
		if len(Items) == 0:
			return
		for it in Items:	
			for ss in allSite:
				if ss[0] == it.text():
					for i in range(self.siteDo.count()):
						if ss[1] == self.siteDo.item(i).text():
							removeSet.append(self.siteDo.item(i))
			for rs in removeSet:
				self.siteDo.takeItem(self.siteDo.row(rs))
			self.utNot.addItem(self.utDo.takeItem(self.utDo.row(it)))
		self.searchUt.clear()
		self.doSiteCheck()
		self.ifListFill()
		self.utNot.sortItems()
		self.utDo.sortItems()
		self.nameNot.sortItems()
		self.nameDo.sortItems()

		
	def removeSiteItem(self):

		it = self.siteDo.selectedItems()
		if len(it) == 0:
			return

		for i in it:
			self.siteNot.addItem(self.siteDo.takeItem(self.siteDo.row(i)))
		self.searchSite.clear()
		self.ifListFill()
		self.siteNot.sortItems()
		self.siteDo.sortItems()

#remove all function
	def removeAllUT(self):
		for i in range(self.utDo.count()):
			self.utNot.addItem(self.utDo.takeItem(0))
			self.nameNot.addItem(self.nameDo.takeItem(0))
		self.siteDo.clear()
		self.siteNot.clear()
		self.ifListFill()
		self.nameNot.sortItems()
		self.nameDo.sortItems()

	
	def removeAllSIte(self):
		for i in range(self.siteDo.count()):
			self.siteNot.addItem(self.siteDo.takeItem(0))
		self.ifListFill()
		self.siteNot.sortItems()
		self.siteDo.sortItems()

#The main function
if __name__ == '__main__':
	app = QApplication([sys.argv[0], "-platform", "windows:dpiawareness=0"])
	ex = MyApp()
	ex.show()
	sys.exit(app.exec_())