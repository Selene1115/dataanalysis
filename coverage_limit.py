from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *


#the number of sites with coverage greater than a specified value and with a valid index exceeding
# the relevant limit(s) SHALL be calculated.
def coverage_limit(utilityTmp, index, result_set_id):
    cursor = result_cnn.cursor()
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    period = "Annual"

    #for LV site
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # find limit
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    limit = float(row[0])

    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    site = []
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)  # filter site whose coverage is bigger than 0.25
        if filtered_site:
            select_stmt = "SELECT value FROM site_result WHERE index_id = (%%s) AND result_set_id = (%%s) " \
                          "AND period = (%%s) AND site_id IN (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [index, result_set_id, period] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            LV_count = 0
            while row is not None:
                if float(row[0]) > limit:
                    LV_count = LV_count + 1
                row = cursor.fetchone()

            index = "LV count"
            period = "Annual"
            statistic = "LV coverage with over limit"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                          " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                          "%(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                 'index_id': index, 'statistic_id': statistic,
                                                 'period': period, 'value': float(LV_count)})
            result_cnn.commit()
            another_cursor.close()

    # for MV site
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    site = []
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)  # filter site whose coverage is bigger than 0.25
        if filtered_site:
            #count the number based on different nominal voltage
            count1 = MV_cl_limit(juristiction, utilityTmp, result_set_id, index, 6.6, filtered_site)
            count2 = MV11_cl_limit(juristiction, utilityTmp, result_set_id, index, filtered_site)
            count3 = MV_cl_limit(juristiction, utilityTmp, result_set_id, index, 33, filtered_site)
            count4 = MV_cl_limit(juristiction, utilityTmp, result_set_id, index, 66, filtered_site)
            count5 = MV_cl_limit(juristiction, utilityTmp, result_set_id, index, 132, filtered_site)
            MV_count = count1 + count2 + count3 + count4 + count5   # the total is the sum of these counts
            index = "LV count"
            period = "Annual"
            statistic = "LV coverage with over limit"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                          " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                          "%(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                 'index_id': index, 'statistic_id': statistic,
                                                 'period': period, 'value': float(MV_count)})
            result_cnn.commit()
            another_cursor.close()


#For MV sites the number of sites in the following nominal voltage categories SHALL also be calculated: 6.6 kV,
# 11/22 kV, 33 kV, 66 kV, 133 kV
#count the number of value site coverage bigger than specific value but over limit
def MV_cl_limit(juristiction, utilityTmp, result_set_id, index, nominal, site):    # not for 11/22, site is MV site
    cursor = result_cnn.cursor()
    nominal_site = []
    MV_count = 0
    period = "Annual"
    nominal_voltage = str(nominal * 1000)
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = [nominal_voltage] + site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:      #find the site with this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()
    if nominal_site:
        nominal_voltage = nominal * 1000
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominal_voltage})  # limit
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominal_voltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        final_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    final_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                final_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            final_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) " \
                      "AND period = 'Annual' AND index_id = (%%s) " \
                      "AND site_id in (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_site)))
        select_stmt = select_stmt % in_p
        arg = [result_set_id, index] + nominal_site
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        while row is not None:
            if float(row[0]) > final_limit:
                MV_count = MV_count + 1
            row = cursor.fetchone()
        index = index + " count"
        period = "Annual"
        statistic = "MV coverage with limit " + str(nominal)
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(MV_count)})
        result_cnn.commit()
        cursor.close()
    return MV_count


def MV11_cl_limit(juristiction, utilityTmp, result_set_id, index, site):  # for 11/22, site is MV site
    cursor = result_cnn.cursor()
    nominal_site = []
    period = "Annual"
    MV_count = 0
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = '11000' AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:  # find the site with this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()
    if nominal_site:   #find the sites with 11/22 kV
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': "11000"})  # limit for 11kv
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': "11000"})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        final_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    final_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                final_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            final_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) " \
                      "AND period = 'Annual' AND index_id = (%%s) AND site_id in (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_site)))
        select_stmt = select_stmt % in_p
        arg = [result_set_id, index] + nominal_site
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        while row is not None:
            if float(row[0]) > final_limit:
                MV_count = MV_count + 1
            row = cursor.fetchone()

    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = '22000' AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    nominal_site = []
    while row is not None:  # find the site with this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()
    if nominal_site:
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': "22000"})  # limit for 22kv
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': "22000"})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        final_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    final_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                final_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            final_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        select_stmt = "SELECT value FROM site_result WHERE result_set_id = (%%s) " \
                      "AND period = 'Annual' AND index_id = (%%s) " \
                      "AND site_id in (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_site)))
        select_stmt = select_stmt % in_p
        arg = [result_set_id, "22000", index] + nominal_site
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        while row is not None:
            if float(row[0]) > final_limit:
                MV_count = MV_count + 1
            row = cursor.fetchone()

    index = index + " count"
    period = "Annual"
    statistic = "MV coverage with limit 11/22"
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                 'index_id': index, 'statistic_id': statistic,
                                 'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    cursor.close()
    return MV_count

