# This file has calcluation of Index_Statistics_By_Class
# PCL at utility level
# Performance_By_Class
# PNV
__author__ = {'name' : 'Changze Huang'}
import numpy as np
import datetime
import math
from database_connections import *


# 3.3.16.4 index statistic by site classification
# this function calcluate without feeder_category and load
#THis function has filter the coverage
def utility_class (utilityTmp, index_id, Strength, voltage_level, month, percentile, result_set_id):
	
	
	res = []  # for annual

	cursor = cnn.cursor()
	select_stmt =   "SELECT value FROM PQAWeb.site_result WHERE index_id = %(index)s and period = %(month)s AND result_set_id = '2015-2016 Full Run' AND site_id IN (select site_id from PQAWeb.site_result where index_id = 'Coverage'and period = %(month)s and value > 0.25 and site_id in (SELECT site_id FROM PQAWeb.site WHERE site_utility = %(utility)s AND voltage_level = %(vLevel)s and strength = %(Strength)s))"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp, 'vLevel': voltage_level, 'Strength': Strength, 'month': month});
	row = cursor.fetchone()
	while row is not None:
		res.append(float(row[0]))
		row = cursor.fetchone()
		
	if (len(res) == 0):
		return None
	elif (len(res) == 1):
		result = res[0]
	else:
		result = np.percentile(res, percentile)

	another_cursor = result_cnn.cursor()
	insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
	another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': voltage_level + " " + str(percentile) + "%",
												 'period': month, 'value': float(result)})
	result_cnn.commit()
	another_cursor.close()
	
	return res


# 3.3.16.4 index statistic by site classification
# this function calcluate with feeder_category 
#THis function has filter the coverage
	
def utility_class_f(utilityTmp, index_id, Strength, voltage_level, feeder_category, month, percentile, result_set_id):
	cursor = cnn.cursor()
	
	res = []  # for annual
	
	select_stmt =   "SELECT value FROM PQAWeb.site_result WHERE index_id = %(index)s and period = %(month)s AND result_set_id = '2015-2016 Full Run' AND site_id IN (select site_id from PQAWeb.site_result where index_id = 'Coverage'and period = %(month)s and value > 0.25 and site_id in (SELECT site_id FROM PQAWeb.site WHERE site_utility = %(utility)s AND voltage_level = %(vLevel)s and strength = %(Strength)s and feeder_category = %(feeder)s))"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp, 'vLevel': voltage_level, 'Strength': Strength, 'feeder': feeder_category, 'month': month});
	row = cursor.fetchone()
	while row is not None:
		res.append(float(row[0]))
		row = cursor.fetchone()
	
	if row == None:
		return

	if (len(res) == 0):
		return None
	elif (len(res) == 1):
		result = res[0]
	else:
		result = np.percentile(res, percentile)

	
	res = np.percentile(res, percentile)
	another_cursor = result_cnn.cursor()
	insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
	another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': voltage_level + " " + str(percentile) + "%",
												 'period': month, 'value': float(res)})
	result_cnn.commit()
	another_cursor.close()

	
	
	return res

	
# 3.3.16.4 index statistic by site classification
# this function calcluate withload
#THis function has filter the coverage

def utility_class_l(utilityTmp, index_id, Strength, voltage_level, load, month, percentile, result_set_id):
	cursor = cnn.cursor()
	
	res = []  # for annual
	
	select_stmt =   "SELECT value FROM PQAWeb.site_result WHERE index_id = %(index)s and period = %(month)s AND result_set_id = '2015-2016 Full Run' AND site_id IN (select site_id from PQAWeb.site_result where index_id = 'Coverage'and period = %(month)s and value > 0.25 and site_id in (SELECT site_id FROM PQAWeb.site WHERE site_utility = %(utility)s AND voltage_level = %(vLevel)s and strength = %(Strength)s and load_type = %(feeder)s))"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp, 'vLevel': voltage_level, 'Strength': Strength, 'feeder': load, 'month': month});
	row = cursor.fetchone()
	while row is not None:
		res.append(float(row[0]))
		row = cursor.fetchone()
	
	if row == None:
		return

	if (len(res) == 0):
		return None
	elif (len(res) == 1):
		result = res[0]
	else:
		result = np.percentile(res, percentile)

	
	res = np.percentile(res, percentile)
	another_cursor = result_cnn.cursor()
	insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
	another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': voltage_level + " " + str(percentile) + "%",
												 'period': month, 'value': float(res)})
	result_cnn.commit()
	another_cursor.close()

	return res


# 3.3.16.5 percentage of limit values
# This function can only do LV site
#This funtion DOES NOT filter the coverage	
def PCL_utility(utilityTmp, index_id, month, result_set_id):      ####need add coverage
	if index_id == 'PI':
		return None
	
	if index_id == 'V99' or index_id == 'V1':
		return None

####Do LV first
	cursor = cnn.cursor()
	select_stmt1 = "select value from PQAWeb.jurisfictional_limits where index_name = %(index)s and voltage_level = 'LV' and jurisdiction_id = (select utility_jurisdiction_id from PQAWeb.utility where utility_id = %(utility)s)"
	cursor.execute(select_stmt1, {'index': index_id, 'utility': utilityTmp})
	value = cursor.fetchone()
	if value == None:
		return None


	cursor = cnn.cursor()
	select_stmt = "select * from PQAWeb.utility_result where utility_id = %(utility)s and index_id = %(index)s and statistic_id like 'LV%' and statistic_id REGEXP '[0-9]' and result_set_id = '2015-2016 Full Run'"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp})
	row = cursor.fetchone()
	
	num = ''
	while row is not None:
		name = ''
		name = name.join(row[3]) + ' PCI'

		num = row[5]

		num = float(num)
		num = num / float(value[0])

		row = cursor.fetchone()
	
		if row == None:
			return
		another_cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
		another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': name,
												 'period': month, 'value': num})
		result_cnn.commit()

	if index_id == 'AVDI':
		return 1

		
#This function find the normalised value
def normal(val, index_id, lim):
	temp = val/lim
	if index_id == 'AVDI':
		return val/lim
	elif index_id == 'VUFI':
		return temp ** 2
	elif index_id == 'THDI':
		return temp ** 2
	elif index_id == 'PSTI':
		return temp ** 3
	elif index_id == 'PLTI':
		return temp ** 3
	elif index_id == 'SI':
		return temp


# 3.3.16.6 performance by site classification
#This function can only do without feed and load
#This funtion DOES NOT filter the coverage	

def perf_utility_class(utilityTmp, index_id, Strength, voltage_level, result_set_id):
	if index_id == 'V99' or index_id == 'V1':
		return
	if (voltage_level == 'MV' and index_id == 'AVDI'):
		return
	if (voltage_level == 'LV' and index_id == 'VSpread'):
		return
		
	if (voltage_level == 'LV' and index_id == 'Spread'):
		return

	cursor = cnn.cursor()
	select_stmt1 = "select value from PQAWeb.jurisfictional_limits where index_name = %(index)s and voltage_level = %(level)s and nominal_voltage is NULL and jurisdiction_id = (select utility_jurisdiction_id from PQAWeb.utility where utility_id = %(utility)s)"
	cursor.execute(select_stmt1, {'index': index_id, 'utility': utilityTmp, 'level': voltage_level})
	fee = cursor.fetchone()
	if fee == None:
		return None
	lim = float(fee[0])

	res = []
	cursor = cnn.cursor()
	select_stmt =   "SELECT value FROM PQAWeb.site_result WHERE index_id = %(index)s AND result_set_id = '2015-2016 Full Run' AND site_id IN (select site_id from PQAWeb.site_result where index_id = 'Coverage' and value > 0.25 and site_id in (SELECT site_id FROM PQAWeb.site WHERE site_utility = %(utility)s AND voltage_level = %(vLevel)s and strength = %(Strength)s))"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp, 'vLevel': voltage_level, 'Strength': Strength});
	row = cursor.fetchone()
	while row is not None:
		res.append(float(normal(float(row[0]), index_id, lim)))
		row = cursor.fetchone()
		
	num = -1
	if (len(res) == 0):
		return None
	elif (len(res) == 1):
		num = res[0]
	else:
		num = np.mean(res)
		num = num / len(res)

	if num != -1:

		another_cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
		another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': 'perf_utility_class',
												 'period': 'Annual', 'value': float(num)})
		result_cnn.commit()



#3.3.16.9 Predicted network value
#This funtion DOES NOT filter the coverage	

def predicted_network(utilityTmp, index_id, result_set_id):
	
	resultSet = []

	cursor = cnn.cursor()
	select_stmt =   "SELECT value FROM PQAWeb.site_result WHERE index_id = %(index)s  AND result_set_id = '2015-2016 Full Run' AND site_id IN (select site_id from PQAWeb.site_result where index_id = 'Coverage' and value > 0.25 and site_id in (SELECT site_id FROM PQAWeb.site WHERE site_utility = %(utility)s))"
	cursor.execute(select_stmt, {'index': index_id, 'utility': utilityTmp});
	row = cursor.fetchone()
	while row is not None:
		resultSet.append(float(row[0]))
		row = cursor.fetchone()

	if (len(resultSet) == 1 or len(resultSet) == 0):
		return
	x_bar = np.mean(resultSet)
	s = np.std(resultSet)
	n  = len(resultSet)
	if x_bar == None or s == None or n == None:
		return None
	
	#This function check value mannually and cannot change
	z_lambda = 1.959964
	z_p = 0.82894

	a = 1 - ((z_lambda ** 2) / 2 * (n - 1))
	b = z_p ** 2 - (z_lambda ** 2) / n
	if ((z_p ** 2) - a * b) <= 0:
		return
	temp = np.sqrt((z_p ** 2) - a * b)
	K = (z_p - temp) / a
	if index_id == 'V1':
		r = x_bar + K * s
		another_cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
		another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': 'predicted_network',
												 'period': 'Annual', 'value': str(r)})
		result_cnn.commit()
		return x_bar + K * s
	else:
		r = x_bar - K * s
		another_cursor = result_cnn.cursor()
		insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
						  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
						  "%(value)s)"
		another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
												 'index_id': index_id, 'statistic_id': 'predicted_network',
												 'period': 'Annual', 'value': str(r)})
		result_cnn.commit()

		return x_bar - K * s
		
	
#testing function here...
if __name__ == '__main__':
	
	utilityTmp = 'ZB'

	mn = ['Annual', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'Augest', 'September', 'October', 'December']
	indexListUt = ['V99', 'V1', 'VSpread', 'AVDI', 'VUFI', 'THDI', 'HCI', 'PSTI', 'PLTI', 'SI']
	feeder = ['CBD', 'Urban', 'Short rural', 'Long rural']
	load = ['Residential', 'Commercial', 'Industrial', 'Mixed']
	hxi = []
	for i in range(25):
		hxi.append("H" + str(i + 1) + "I")


	

	# for m in mn:
	# for iL in indexListUt:
	perf_utility_class('ZG', 'Spread', 'Strong', 'LV', 'full run')
	#         utility_class('ZB', iL, 'Strong', 'MV', m, 95, 'Tf')
	#         for fe in feeder:   
	#             utility_class_f('ZB', iL, 'Strong', 'MV', fe, m, 50, 'Tf')
	#         for lo in load:
	#             utility_class_l('ZB', iL, 'Strong', 'MV', lo, m, 95, 'Tf')


	#     for hi in hxi:
	#         utility_class('ZB', iL, 'Strong', 'MV', m, 95, 'Tf')
	#         for fe in feeder:   
	#             utility_class_f('ZB', iL, 'Strong', 'MV', fe, m, 50, 'Tf')
	#         for lo in load:
	#             utility_class_l('ZB', iL, 'Strong', 'MV', lo, m, 95, 'Tf')
	# utility_class_f('ZA', 'V1', 'Strong', 'LV', 'Ubran','Annual', 50, 'a')
	#perf_utility_class_f('ZA', 'Strong', 'MV', 'a')






