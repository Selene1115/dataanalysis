from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import atexit
import yaml

conf = open("set.yaml")
x = yaml.load(conf)
user1 = x['readConnection1']['user']
passwd1 = x['readConnection1']['password']
IP1 = x['readConnection1']['ip']
db1 = x['readConnection1']['database']


user2 = x['writeConnection2']['user']
passwd2 = x['writeConnection2']['password']
IP2 = x['writeConnection2']['ip']
db2 =x['writeConnection2']['database']

conf.close()


def connecttion():
    try:
        cnn = mysql.connector.connect(
            user=user1,
            password=passwd1,
            host=IP1,
            database=db1
        )
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with username or password")
            return False
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("database does not exist")
            return False
        else:
            print(err)
            return False

    cnn.close()
    return True


try:
    cnn = mysql.connector.connect(
        user=user1,
        password=passwd1,
        host=IP1,
        database=db1
    )
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with username or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("database does not exist")
    else:
        print(err)

try:
    another_cnn = mysql.connector.connect(
        user=user1,
        password=passwd1,
        host=IP1,
        database=db1
    )
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with username or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("database does not exist")
    else:
        print(err)

try:
    result_cnn = mysql.connector.connect(
        user=user2,
        password=passwd2,
        host=IP2,
        database=db2
    )
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with username or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("database does not exist")
    else:
        print(err)

def exit_handler():
    cnn.close()
    another_cnn.close()
    result_cnn.close()

atexit.register(exit_handler)
