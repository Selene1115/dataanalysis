from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *

def utility_V99_limit(utilityTmp,
                      result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "V99"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for V99
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    V99_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > V99_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for V99
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                    V99_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    V99_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                V99_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            V99_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > V99_limit:
            MV_count = MV_count + 1

    index = "V99 count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "V99"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_V1_limit(utilityTmp,
                     result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "V1"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for V1
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    V1_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > V1_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for V1
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        V1_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    V1_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                V1_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            V1_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > V1_limit:
            MV_count = MV_count + 1

    index = "V1 count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "V1"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_VSpread_limit(utilityTmp,
                          result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "Spread"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for VSpread
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    VSpread_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > VSpread_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for Spread
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        VSpread_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    VSpread_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                VSpread_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            VSpread_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > VSpread_limit:
            MV_count = MV_count + 1

    index = "Spread count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "Spread"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_AVDI_limit(utilityTmp,
                       result_set_id):  # for LV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "AVDI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for AVDI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    AVDI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > AVDI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    index = "AVDI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    result_cnn.commit()
    another_cursor.close()


def utility_VUFI_limit(utilityTmp,
                       result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "VUFI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for VUFI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    VUFI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > VUFI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for VUFI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        VUFI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    VUFI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                VUFI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            VUFI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > VUFI_limit:
            MV_count = MV_count + 1

    index = "VUFI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "VUFI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_THDI_limit(utilityTmp,
                       result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "THDI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for THDI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    THDI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > THDI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for THDI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        THDI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    THDI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                THDI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            THDI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > THDI_limit:
            MV_count = MV_count + 1
    index = "THDI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()

    index = "THDI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_HnI_limit(utilityTmp,
                      result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    for n in range(2, 26):
        index = "H" + str(n) + "I"
        select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
        cursor.execute(select_stmt, {'utility_id': utilityTmp})
        row = cursor.fetchone()
        juristiction = row[0]
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for HnI
        row = cursor.fetchone()
        if row is None:  # get default limit
            default_juristiction = 1
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                          "AND index_name = %(index_name)s AND voltage_level = 'LV'"
            cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
            row = cursor.fetchone()
        HnI_limit = float(row[0])
        LV_count = 0
        select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                      "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                      "AND period = 'Annual' AND index_id = %(index_id)s"
        cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
        row = cursor.fetchone()
        while row is not None:
            if float(row[0]) > HnI_limit:
                LV_count = LV_count + 1
            row = cursor.fetchone()

        MV_count = 0
        select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                      "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                      "AND period = 'Annual' AND index_id = %(index_id)s"
        cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
        row = cursor.fetchone()
        site = []
        value = []
        while row is not None:  # store all the value first
            site.append(row[0])
            value.append(float(row[1]))
            row = cursor.fetchone()
        for i in range(len(site)):
            select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
            cursor.execute(select_stmt, {'site_id': site[i]})
            row = cursor.fetchone()
            nominalVoltage = row[0]
            limit = []  # used for store limit
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage})  # limit for HnI
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                      "jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = 'MV'"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                            HnI_limit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        HnI_limit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                    HnI_limit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                HnI_limit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
            if value[i] > HnI_limit:
                MV_count = MV_count + 1

        index = index + " count"
        period = "Annual"
        statistic = "LV count over limit"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(LV_count)})

        statistic = "MV count over limit"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(MV_count)})
        result_cnn.commit()
        another_cursor.close()
        index = "H" + str(n) + "I"
        #count the number based on different nominal voltage
        MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
        MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
        MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
        MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
        MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_HCI_limit(utilityTmp,
                      result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "HCI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for HCI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    HCI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > HCI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for HCI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        HCI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    HCI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                HCI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            HCI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > HCI_limit:
            MV_count = MV_count + 1

    index = "HCI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "HCI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_PSTI_limit(utilityTmp,
                       result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "PSTI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for PSTI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    PSTI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > PSTI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for PSTI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        PSTI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    PSTI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                PSTI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            PSTI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > PSTI_limit:
            MV_count = MV_count + 1

    index = "PSTI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "PSTI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_PLTI_limit(utilityTmp,
                       result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "PLTI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for PLTI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    PLTI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > PLTI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for PLTI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        PLTI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    PLTI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                PLTI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            PLTI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > PLTI_limit:
            MV_count = MV_count + 1

    index = "PLTI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "PLTI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


def utility_SI_limit(utilityTmp,
                     result_set_id):  # for LV and MV the number of sites with a valid index exceeding the relevant limit(s)
    cursor = result_cnn.cursor()
    index = "SI"
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utilityTmp})
    row = cursor.fetchone()
    juristiction = row[0]
    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and voltage_level = 'LV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})  # limit for SI
    row = cursor.fetchone()
    if row is None:  # get default limit
        default_juristiction = 1
        select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s " \
                      "AND index_name = %(index_name)s AND voltage_level = 'LV'"
        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index})
        row = cursor.fetchone()
    SI_limit = float(row[0])
    LV_count = 0
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > SI_limit:
            LV_count = LV_count + 1
        row = cursor.fetchone()

    MV_count = 0
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV')" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    site = []
    value = []
    while row is not None:  # store all the value first
        site.append(row[0])
        value.append(float(row[1]))
        row = cursor.fetchone()
    for i in range(len(site)):
        select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
        cursor.execute(select_stmt, {'site_id': site[i]})
        row = cursor.fetchone()
        nominalVoltage = row[0]
        limit = []  # used for store limit
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                     'nominal_voltage': nominalVoltage})  # limit for SI
        row = cursor.fetchall()
        if len(row) == 0:  # find the limit for nominal voltage is NULL
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit
                default_juristiction = 1
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index,
                                'nominal_voltage': nominalVoltage})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                                  "jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = 'MV'"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index})
                    row = cursor.fetchall()
                    for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                        default_limit = float(r[0])
                        SI_limit = default_limit
                else:  # has limit for default juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    SI_limit = limit[i - 1]
            else:  # has limit for this juristiction id and nominal voltage is NULL
                for r in row:
                    default_limit = float(r[0])
                SI_limit = default_limit
        else:  # has limit for this juristiction id and nominal voltage is not NULL
            for r in row:
                limit.append(float(r[0]))
            i = len(limit)
            SI_limit = limit[i - 1]
            # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
        if value[i] > SI_limit:
            MV_count = MV_count + 1

    index = "SI count"
    period = "Annual"
    statistic = "LV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(LV_count)})

    statistic = "MV count over limit"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(MV_count)})
    result_cnn.commit()
    another_cursor.close()
    index = "SI"
    #count the number based on different nominal voltage
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 6.6)
    MV11_over_limit(juristiction, utilityTmp, result_set_id, index)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 33)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 66)
    MV_over_limit(juristiction, utilityTmp, result_set_id, index, 132)


#For MV sites the number of sites in the following nominal voltage categories SHALL also be calculated: 6.6 kV,
# 11/22 kV, 33 kV, 66 kV, 133 kV
#count the number of valid index value
def MV_over_limit(juristiction, utilityTmp, result_set_id, index, nominal):    # not for 11/22
    cursor = result_cnn.cursor()
    nominal_voltage = nominal * 1000
    limit = []  # used for store limit
    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                  "jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                  "voltage_level = 'MV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                 'nominal_voltage': nominal_voltage})  # limit for this nominal voltage 
    row = cursor.fetchall()
    if len(row) == 0:  # find the limit for nominal voltage is NULL
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
        row = cursor.fetchall()
        if len(row) == 0:  # find the default limit
            default_juristiction = 1
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt,
                           {'jurisdiction_id': default_juristiction, 'index_name': index,
                            'nominal_voltage': nominal_voltage})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index})
                row = cursor.fetchall()
                for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                    default_limit = float(r[0])
                    final_limit = default_limit
            else:  # has limit for default juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                final_limit = limit[i - 1]
        else:  # has limit for this juristiction id and nominal voltage is NULL
            for r in row:
                default_limit = float(r[0])
            final_limit = default_limit
    else:  # has limit for this juristiction id and nominal voltage is not NULL
        for r in row:
            limit.append(float(r[0]))
        i = len(limit)
        final_limit = limit[i - 1]
        # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV' " \
                  "AND nominal_voltage = %(nominal_voltage)s)" \
                  "AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp,
                                 'nominal_voltage': nominal_voltage, 'index_id': index})
    row = cursor.fetchone()
    count = 0
    while row is not None:
        if float(row[0])>final_limit:
            count = count + 1
        row = cursor.fetchone()
    index = index + " count"
    period = "Annual"
    statistic = "MV count over limit " + str(nominal)
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(count)})
    result_cnn.commit()
    cursor.close()

def MV11_over_limit(juristiction, utilityTmp, result_set_id, index):    # for 11/22
    cursor = result_cnn.cursor()
    limit = []  # used for store limit
    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                  "jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                  "voltage_level = 'MV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                 'nominal_voltage': '11000'})  # limit for 11 kv
    row = cursor.fetchall()
    if len(row) == 0:  # find the limit for nominal voltage is NULL
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
        row = cursor.fetchall()
        if len(row) == 0:  # find the default limit
            default_juristiction = 1
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt,
                           {'jurisdiction_id': default_juristiction, 'index_name': index,
                            'nominal_voltage': '11000'})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index})
                row = cursor.fetchall()
                for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                    default_limit = float(r[0])
                    final_limit = default_limit
            else:  # has limit for default juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                final_limit = limit[i - 1]
        else:  # has limit for this juristiction id and nominal voltage is NULL
            for r in row:
                default_limit = float(r[0])
            final_limit = default_limit
    else:  # has limit for this juristiction id and nominal voltage is not NULL
        for r in row:
            limit.append(float(r[0]))
        i = len(limit)
        final_limit = limit[i - 1]
        # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV' " \
                  "AND nominal_voltage = '11000') AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    count = 0
    while row is not None:
        if float(row[0]) > final_limit:
            count = count + 1
        row = cursor.fetchone()

    limit = []  # used for store limit
    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                  "jurisdiction_id = %(jurisdiction_id)s and " \
                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                  "voltage_level = 'MV'"
    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                 'nominal_voltage': '22000'})  # limit for 22kv
    row = cursor.fetchall()
    if len(row) == 0:  # find the limit for nominal voltage is NULL
        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                      "jurisdiction_id = %(jurisdiction_id)s and " \
                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                      "voltage_level = 'MV'"
        cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index})
        row = cursor.fetchall()
        if len(row) == 0:  # find the default limit
            default_juristiction = 1
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                          "jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = 'MV'"
            cursor.execute(select_stmt,
                           {'jurisdiction_id': default_juristiction, 'index_name': index,
                            'nominal_voltage': '22000'})
            row = cursor.fetchall()
            if len(row) == 0:  # find the default limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE " \
                              "jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = 'MV'"
                cursor.execute(select_stmt,
                               {'jurisdiction_id': default_juristiction, 'index_name': index})
                row = cursor.fetchall()
                for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                    default_limit = float(r[0])
                    final_limit = default_limit
            else:  # has limit for default juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                final_limit = limit[i - 1]
        else:  # has limit for this juristiction id and nominal voltage is NULL
            for r in row:
                default_limit = float(r[0])
            final_limit = default_limit
    else:  # has limit for this juristiction id and nominal voltage is not NULL
        for r in row:
            limit.append(float(r[0]))
        i = len(limit)
        final_limit = limit[i - 1]
        # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id IN (SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV' " \
                  "AND nominal_voltage = '22000') AND period = 'Annual' AND index_id = %(index_id)s"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'utility': utilityTmp, 'index_id': index})
    row = cursor.fetchone()
    while row is not None:
        if float(row[0]) > final_limit:
            count = count + 1
        row = cursor.fetchone()
    index = index + " count"
    period = "Annual"
    statistic = "MV count over limit 11/22"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                  "%(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                         'index_id': index, 'statistic_id': statistic,
                                         'period': period, 'value': float(count)})
    result_cnn.commit()
    another_cursor.close()