from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *

#pass different index to calculate different statistics
def index_statistics(utilityTmp, period, indexTmp, result_set_id):
    cursor = result_cnn.cursor()
    #FOR LV
    site = []
    LV = []
    statistic = []
    statistic.append("LV 0%")
    statistic.append("LV 1%")
    statistic.append("LV 5%")
    statistic.append("LV 10%")
    statistic.append("LV 20%")
    statistic.append("LV 30%")
    statistic.append("LV 40%")
    statistic.append("LV 50%")
    statistic.append("LV 60%")
    statistic.append("LV 70%")
    statistic.append("LV 80%")
    statistic.append("LV 90%")
    statistic.append("LV 95%")
    statistic.append("LV 99%")
    statistic.append("LV 100%")
    statistic.append("LV mean")
    statistic.append("LV standard deviation")
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)   #filter site whose coverage is bigger than 0.25
        if filtered_site:
            select_stmt = "SELECT value FROM site_result WHERE index_id = (%%s) AND result_set_id = (%%s) " \
                          "AND period = (%%s) AND site_id IN (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [indexTmp, result_set_id, period] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                LV.append(float(row[0]))
                row = cursor.fetchone()
            if LV:
                value = []
                value.append(np.percentile(LV, 0))
                value.append(np.percentile(LV, 1))
                value.append(np.percentile(LV, 5))
                value.append(np.percentile(LV, 10))
                value.append(np.percentile(LV, 20))
                value.append(np.percentile(LV, 30))
                value.append(np.percentile(LV, 40))
                value.append(np.percentile(LV, 50))
                value.append(np.percentile(LV, 60))
                value.append(np.percentile(LV, 70))
                value.append(np.percentile(LV, 80))
                value.append(np.percentile(LV, 90))
                value.append(np.percentile(LV, 95))
                value.append(np.percentile(LV, 99))
                value.append(np.percentile(LV, 100))
                value.append(np.mean(LV))
                value.append(np.std(LV))
                index = "utility " + indexTmp
                for i in range(0, 17):   #For each precentile and mean and standard deviation, insert into result table
                    another_cursor = result_cnn.cursor()
                    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                                  "%(value)s)"
                    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                         'index_id': index, 'statistic_id': statistic[i],
                                                         'period': period, 'value': float(value[i])})
                result_cnn.commit()
                another_cursor.close()
                del value

    #FOR MV
    site = []
    MV = []
    statistic = []
    statistic.append("MV 0%")
    statistic.append("MV 1%")
    statistic.append("MV 5%")
    statistic.append("MV 10%")
    statistic.append("MV 20%")
    statistic.append("MV 30%")
    statistic.append("MV 40%")
    statistic.append("MV 50%")
    statistic.append("MV 60%")
    statistic.append("MV 70%")
    statistic.append("MV 80%")
    statistic.append("MV 90%")
    statistic.append("MV 95%")
    statistic.append("MV 99%")
    statistic.append("MV 100%")
    statistic.append("MV mean")
    statistic.append("MV standard deviation")
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)   #filter site whose coverage is bigger than 0.25
        if filtered_site:
            select_stmt = "SELECT value FROM site_result WHERE index_id = (%%s) " \
                          "AND result_set_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [indexTmp, result_set_id, period] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            while row is not None:
                MV.append(float(row[0]))
                row = cursor.fetchone()
            if MV:
                value = []
                value.append(np.percentile(MV, 0))
                value.append(np.percentile(MV, 1))
                value.append(np.percentile(MV, 5))
                value.append(np.percentile(MV, 10))
                value.append(np.percentile(MV, 20))
                value.append(np.percentile(MV, 30))
                value.append(np.percentile(MV, 40))
                value.append(np.percentile(MV, 50))
                value.append(np.percentile(MV, 60))
                value.append(np.percentile(MV, 70))
                value.append(np.percentile(MV, 80))
                value.append(np.percentile(MV, 90))
                value.append(np.percentile(MV, 95))
                value.append(np.percentile(MV, 99))
                value.append(np.percentile(MV, 100))
                value.append(np.mean(MV))
                value.append(np.std(MV))
                index = "utility " + indexTmp
                for i in range(0, 17):        #For each precentile and mean and standard deviation, insert into result table
                    another_cursor = result_cnn.cursor()
                    insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                                  " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                                  "%(value)s)"
                    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                         'index_id': index, 'statistic_id': statistic[i],
                                                         'period': period, 'value': float(value[i])})
                result_cnn.commit()
                another_cursor.close()
                del value
            MV_nomina_voltage(6.6, filtered_site, indexTmp, result_set_id, period, utilityTmp)
            MV_11("11/22", filtered_site, indexTmp, result_set_id, period, utilityTmp)
            MV_nomina_voltage(33, filtered_site, indexTmp, result_set_id, period, utilityTmp)
            MV_nomina_voltage(66, filtered_site, indexTmp, result_set_id, period, utilityTmp)
            MV_nomina_voltage(132, filtered_site, indexTmp, result_set_id, period, utilityTmp)

def MV_nomina_voltage(nominal_voltage, MV_site, indexTmp, result_set_id, period, utilityTmp):   #For MV the utility level statistics in the following nominal voltage categories SHALL also be calculated: 6.6 kV, 33 kV, 66 kV, 133 kV.
    statistic = []
    statistic.append(str(nominal_voltage) + "MV 0%")
    statistic.append(str(nominal_voltage) + "MV 1%")
    statistic.append(str(nominal_voltage) + "MV 5%")
    statistic.append(str(nominal_voltage) + "MV 10%")
    statistic.append(str(nominal_voltage) + "MV 20%")
    statistic.append(str(nominal_voltage) + "MV 30%")
    statistic.append(str(nominal_voltage) + "MV 40%")
    statistic.append(str(nominal_voltage) + "MV 50%")
    statistic.append(str(nominal_voltage) + "MV 60%")
    statistic.append(str(nominal_voltage) + "MV 70%")
    statistic.append(str(nominal_voltage) + "MV 80%")
    statistic.append(str(nominal_voltage) + "MV 90%")
    statistic.append(str(nominal_voltage) + "MV 95%")
    statistic.append(str(nominal_voltage) + "MV 99%")
    statistic.append(str(nominal_voltage) + "MV 100%")
    statistic.append(str(nominal_voltage) + "MV mean")
    statistic.append(str(nominal_voltage) + "MV standard deviation")
    cursor = result_cnn.cursor()
    nominal_voltage = nominal_voltage * 1000
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', MV_site)))
    select_stmt = select_stmt % in_p
    arg = [nominal_voltage] + MV_site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    nominal_voltage_list = []
    MV = []
    while row is not None:
        nominal_voltage_list.append(row[0])    #store all the site id which belongs to this nominal voltage category
        row = cursor.fetchone()
    if nominal_voltage_list:
        select_stmt = "SELECT value FROM site_result WHERE index_id = (%%s) " \
                      "AND result_set_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_voltage_list)))
        select_stmt = select_stmt % in_p
        arg = [indexTmp, result_set_id, period] + nominal_voltage_list
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        while row is not None:       #fetch the value belongs to this index
            MV.append(float(row[0]))
            row = cursor.fetchone()
        if MV:
            value = []
            value.append(np.percentile(MV, 0))
            value.append(np.percentile(MV, 1))
            value.append(np.percentile(MV, 5))
            value.append(np.percentile(MV, 10))
            value.append(np.percentile(MV, 20))
            value.append(np.percentile(MV, 30))
            value.append(np.percentile(MV, 40))
            value.append(np.percentile(MV, 50))
            value.append(np.percentile(MV, 60))
            value.append(np.percentile(MV, 70))
            value.append(np.percentile(MV, 80))
            value.append(np.percentile(MV, 90))
            value.append(np.percentile(MV, 95))
            value.append(np.percentile(MV, 99))
            value.append(np.percentile(MV, 100))
            value.append(np.mean(MV))
            value.append(np.std(MV))
            index = indexTmp + " for " + str(nominal_voltage)
            for i in range(0, 17):     #For each precentile and mean and standard deviation, insert into result table
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                              " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                              "%(value)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                     'index_id': index, 'statistic_id': statistic[i],
                                                     'period': period, 'value': float(value[i])})
            result_cnn.commit()
            another_cursor.close()
            del value

def MV_11(nominal_voltage, MV_site, indexTmp, result_set_id, period, utilityTmp):   #For MV the utility level statistics in 11/22KV.
    statistic = []
    statistic.append(nominal_voltage + "MV 0%")
    statistic.append(nominal_voltage + "MV 1%")
    statistic.append(nominal_voltage + "MV 5%")
    statistic.append(nominal_voltage + "MV 10%")
    statistic.append(nominal_voltage + "MV 20%")
    statistic.append(nominal_voltage + "MV 30%")
    statistic.append(nominal_voltage + "MV 40%")
    statistic.append(nominal_voltage + "MV 50%")
    statistic.append(nominal_voltage + "MV 60%")
    statistic.append(nominal_voltage + "MV 70%")
    statistic.append(nominal_voltage + "MV 80%")
    statistic.append(nominal_voltage + "MV 90%")
    statistic.append(nominal_voltage + "MV 95%")
    statistic.append(nominal_voltage + "MV 99%")
    statistic.append(nominal_voltage + "MV 100%")
    statistic.append(nominal_voltage + "MV mean")
    statistic.append(nominal_voltage + "MV standard deviation")
    cursor = result_cnn.cursor()
    nominal_voltage = 11 * 1000
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', MV_site)))
    select_stmt = select_stmt % in_p
    arg = [nominal_voltage] + MV_site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    nominal_voltage_list = []
    MV = []
    while row is not None:
        nominal_voltage_list.append(row[0])    #store all the site id which belongs to 11KV
        row = cursor.fetchone()
    nominal_voltage = 22 * 1000
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', MV_site)))
    select_stmt = select_stmt % in_p
    arg = [nominal_voltage] + MV_site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:
        nominal_voltage_list.append(row[0])    #store all the site id which belongs to 22KV
        row = cursor.fetchone()
    if nominal_voltage_list:
        select_stmt = "SELECT value FROM site_result WHERE index_id = (%%s) " \
                      "AND result_set_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_voltage_list)))
        select_stmt = select_stmt % in_p
        arg = [indexTmp, result_set_id, period] + nominal_voltage_list
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        while row is not None:               #fetch value belongs to this index
            MV.append(float(row[0]))
            row = cursor.fetchone()
        if MV:
            value = []
            value.append(np.percentile(MV, 0))
            value.append(np.percentile(MV, 1))
            value.append(np.percentile(MV, 5))
            value.append(np.percentile(MV, 10))
            value.append(np.percentile(MV, 20))
            value.append(np.percentile(MV, 30))
            value.append(np.percentile(MV, 40))
            value.append(np.percentile(MV, 50))
            value.append(np.percentile(MV, 60))
            value.append(np.percentile(MV, 70))
            value.append(np.percentile(MV, 80))
            value.append(np.percentile(MV, 90))
            value.append(np.percentile(MV, 95))
            value.append(np.percentile(MV, 99))
            value.append(np.percentile(MV, 100))
            value.append(np.mean(MV))
            value.append(np.std(MV))
            index = indexTmp + " for 11/22kV"
            for i in range(0, 17):      #For each precentile and mean and standard deviation, insert into result table
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                              " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                              "%(value)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                     'index_id': index, 'statistic_id': statistic[i],
                                                     'period': period, 'value': float(value[i])})
            result_cnn.commit()
            another_cursor.close()
            del value
