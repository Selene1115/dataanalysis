from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *
from coverage_filter import *

# coverage greater than a specified value and a valid index
def coverage_valid(utilityTmp, indexTmp, result_set_id):
    cursor = result_cnn.cursor()
    period = "Annual"
    site = []
    #for LV
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'LV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)  # filter site whose coverage is bigger than 0.25
        if filtered_site:
            select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = (%%s)" \
                          "AND index_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [result_set_id, indexTmp, period] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            LV_count = row[0]

            index = indexTmp + " count"
            period = "Annual"
            statistic = "LV coverage with valid"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                          " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                          "%(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                 'index_id': index, 'statistic_id': statistic,
                                                 'period': period, 'value': float(LV_count)})

    # for MV
    select_stmt = "SELECT site_id FROM site WHERE site_utility = %(utility)s AND voltage_level = 'MV'"
    cursor.execute(select_stmt, {'utility': utilityTmp})
    row = cursor.fetchone()
    site = []
    while row is not None:
        site.append(row[0])
        row = cursor.fetchone()
    if site:
        filtered_site = filterSite(site, period, result_set_id)  # filter site whose coverage is bigger than 0.25
        if filtered_site:
            select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = (%%s)" \
                          "AND index_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
            in_p = ", ".join(list(map(lambda x: '%s', filtered_site)))
            select_stmt = select_stmt % in_p
            arg = [result_set_id, indexTmp, period] + filtered_site
            cursor.execute(select_stmt, arg)
            row = cursor.fetchone()
            MV_count = row[0]

            statistic = "MV coverage with valid"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                          " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                          "%(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                                 'index_id': index, 'statistic_id': statistic,
                                                 'period': period, 'value': float(MV_count)})
            result_cnn.commit()
            another_cursor.close()
            #count the number based on different nominal voltage
            nominal_coverage_count(filtered_site, result_set_id, indexTmp, 6.6, utilityTmp)
            nominal11_coverage_count(filtered_site, result_set_id, indexTmp, utilityTmp)
            nominal_coverage_count(filtered_site, result_set_id, indexTmp, 33, utilityTmp)
            nominal_coverage_count(filtered_site, result_set_id, indexTmp, 66, utilityTmp)
            nominal_coverage_count(filtered_site, result_set_id, indexTmp, 132, utilityTmp)

#For MV sites the number of sites in the following nominal voltage categories SHALL also be calculated: 6.6 kV,
# 11/22 kV, 33 kV, 66 kV, 133 kV
#count the number of valid index value but site coverage biiger than specific value

def nominal_coverage_count(site, result_set_id, index, nominal, utilityTmp):   #not for 11/22 kV
    cursor = result_cnn.cursor()
    nominal_site = []
    period = "Annual"
    nominal_voltage = str(nominal * 1000)
    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = [nominal_voltage] + site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:      #find the site with this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()
    if nominal_site:
        select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = (%%s) AND index_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_site)))
        select_stmt = select_stmt % in_p
        arg = [result_set_id, index, period] + nominal_site
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        MV_count = row[0]

        index = index + " count"
        period = "Annual"
        statistic = "MV coverage with valid " + str(nominal)
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(MV_count)})
        result_cnn.commit()
        another_cursor.close()


def nominal11_coverage_count(site, result_set_id, index, utilityTmp):  # for 11/22 kV
    cursor = result_cnn.cursor()
    nominal_site = []
    period = "Annual"

    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = '11000' AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:  # find the site belongs to this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()

    select_stmt = "SELECT site_id FROM site WHERE nominal_voltage = '22000' AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:  # find the site belongs to this nominal voltage
        nominal_site.append(row[0])
        row = cursor.fetchone()

    if nominal_site:
        select_stmt = "SELECT COUNT(DISTINCT site_id) FROM site_result WHERE result_set_id = (%%s) " \
                      "AND index_id = (%%s) AND period = (%%s) AND site_id IN (%s)"
        in_p = ", ".join(list(map(lambda x: '%s', nominal_site)))
        select_stmt = select_stmt % in_p
        arg = [result_set_id, index, period] + nominal_site
        cursor.execute(select_stmt, arg)
        row = cursor.fetchone()
        MV_count = row[0]

        index = index + " count"
        period = "Annual"
        statistic = "MV coverage with valid 11/22"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO utility_result (result_set_id, utility_id, index_id, statistic_id, period, value)" \
                      " VALUES (%(result_set_id)s, %(utility_id)s, %(index_id)s, %(statistic_id)s, %(period)s, " \
                      "%(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'utility_id': utilityTmp,
                                             'index_id': index, 'statistic_id': statistic,
                                             'period': period, 'value': float(MV_count)})
        result_cnn.commit()
        another_cursor.close()