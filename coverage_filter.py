from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math

#remove the site whose coverage is bigger than 0.25
def filterSite(site, period, result_set_id):
    filtered_site = []
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_id, value FROM site_result WHERE result_set_id = (%%s) AND " \
                  "index_id = 'Coverage' AND period = (%%s) AND site_id IN (%s)"
    in_p = ", ".join(list(map(lambda x: '%s', site)))
    select_stmt = select_stmt % in_p
    arg = [result_set_id, period] + site
    cursor.execute(select_stmt, arg)
    row = cursor.fetchone()
    while row is not None:
        if float(row[1]) > 0.25:
           filtered_site.append(row[0])
        row = cursor.fetchone()
    return filtered_site