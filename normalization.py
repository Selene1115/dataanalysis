from database_connections import *
import mysql.connector
from mysql.connector import errorcode

def cvolt2Volt(mvolt,siteTmp):   #mvolt is a list, continuous voltages to volts
    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    cursor = cnn.cursor()
    select_stmt = "SELECT continuous_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()  # continous scaling factor from instrument database
    if row is not None:
        cscalFactor = row[0]
    else:
        cscalFactor = 1;
    normalized = []
    for volt in mvolt:
        result = volt * cscalFactor
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized

def dvolt2Volt(mvolt, siteTmp):    #mvolt is a list, discrete voltages to volts
    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    cursor = cnn.cursor()
    select_stmt = "SELECT discrete_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()  # discrete scaling factor from instrument database
    if row is not None:
        dscalFactor = row[0]
    else:
        dscalFactor = 1
    normalized = []
    for volt in mvolt:
        result = volt * dscalFactor
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized
    
def percent2Volt(mvolt, siteTmp):
    cursor = cnn.cursor()
    select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    nominalVoltage = row[0]   # nominal volatege comes from site database depending on which site
    normalized = []
    for percentage in mvolt:
        result = percentage * nominalVoltage
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized

def pu2Volt_list(mvolt, siteTmp):    #for a list to convert to volt, pu to volts
    cursor = cnn.cursor()
    select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    nominalVoltage = row[0]  # nominal volatege comes from site database depending on which site
    normalized = []
    for pu in mvolt:
        result = pu * nominalVoltage
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized

def pu2Volt(mvolt, siteTmp):     # for a single value to convert to volt  
    cursor = cnn.cursor()
    select_stmt = "SELECT nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    nominalVoltage = row[0]  # nominal volatege comes from site database depending on which site
    result = mvolt * nominalVoltage
    cnn.commit()
    cursor.close()
    return result

def cvolt2Pu(mvolt, siteTmp):    #mvolt is the data from databse, continous voltages to pu
    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    cursor = cnn.cursor()
    select_stmt = "SELECT continuous_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"  # continous scaling factor from instrument database
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()
    if row is not None:
        cscalFactor = row[0]
    else:
        cscalFactor = 1
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    refVoltage = row[0]      # reference volatege comes from site database depending on which site
    normalized = []
    for volt in mvolt:
        result = volt * cscalFactor / refVoltage
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized

def volt2Pu(mvolt, siteTmp):    #mvolt is the data normalized to volt
    cursor = cnn.cursor()
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    refVoltage = row[0]     # reference volatege comes from site database depending on which site
    normalized = []
    for volt in mvolt:
        result = volt / refVoltage
        normalized.append(result)
    cnn.commit()
    cursor.close()
    return normalized

def dvolt2Pu(mvolt, siteTmp):
    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    cursor = another_cnn.cursor()
    select_stmt = "SELECT discrete_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"  # continous scaling factor from instrument database
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()
    if row is not None:
        dscalFactor = row[0]
    else:
        dscalFactor = 1
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    refVoltage = row[0]  # reference volatege comes from site database depending on which site
    result = mvolt * dscalFactor / refVoltage
    another_cnn.commit()
    cursor.close()
    return result

def percentLV2Pu(mvolt):    #LV site percentage to pu
    result = mvolt / 100
    return result

def percentMV2Pu(mvolt, siteTmp):   #MV site percentage to pu
    cursor = another_cnn.cursor()
    select_stmt = "SELECT reference_voltage, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    refVoltage = row[0]  # reference volatege comes from site database depending on which site
    nominalVoltage = row[1]  # nominal volatege comes from site database depending on which site
    result = (mvolt / 100) * (nominalVoltage / refVoltage)
    another_cnn.commit()
    cursor.close()
    return result