from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from site_indices import *
from normalization import *
from filter import *

#different bin label for flicker, return the (bin label-1)
def bin_flicker(flicker):
    if flicker < 0.1:
        i = 0
    if flicker >= 0.1 and flicker < 0.2:
        i = 1
    if flicker >= 0.2 and flicker < 0.3:
        i = 2
    if flicker >= 0.3 and flicker < 0.4:
        i = 3
    if flicker >= 0.4 and flicker < 0.5:
        i = 4
    if flicker >= 0.5 and flicker < 0.6:
        i = 5
    if flicker >= 0.6 and flicker < 0.7:
        i = 6
    if flicker >= 0.7 and flicker < 0.8:
        i = 7
    if flicker >= 0.8 and flicker < 0.9:
        i = 8
    if flicker >= 0.9 and flicker < 1.0:
        i = 9
    if flicker >= 1.0 and flicker < 1.1:
        i = 10
    if flicker >= 1.1 and flicker < 1.2:
        i = 11
    if flicker >= 1.2 and flicker < 1.3:
        i = 12
    if flicker >= 1.3 and flicker < 1.4:
        i = 13
    if flicker >= 1.4 and flicker < 1.5:
        i = 14
    if flicker >= 1.5 and flicker < 1.6:
        i = 15
    if flicker >= 1.6 and flicker < 1.7:
        i = 16
    if flicker >= 1.7 and flicker < 1.8:
        i = 17
    if flicker >= 1.8 and flicker < 1.9:
        i = 18
    if flicker >= 1.9 and flicker < 2.0:
        i = 19
    if flicker >= 2.0:
        i = 20
    return i

#histogram for Short term flicker
def histogram_PST(siteTmp, result_set_id):       
    bin = []     # a list stores the number of Short term flickers in that bin
    label = []
    label.append("< 0.1")
    label.append("0.1-0.2")
    label.append("0.2-0.3")
    label.append("0.3-0.4")
    label.append("0.4-0.5")
    label.append("0.5-0.6")
    label.append("0.6-0.7")
    label.append("0.7-0.8")
    label.append("0.8-0.9")
    label.append("0.9-1.0")
    label.append("1.0-1.1")
    label.append("1.1-1.2")
    label.append("1.2-1.3")
    label.append("1.3-1.4")
    label.append("1.4-1.5")
    label.append("1.5-1.6")
    label.append("1.6-1.7")
    label.append("1.7-1.8")
    label.append("1.8-1.9")
    label.append("1.9-2.0")
    label.append("> 2.0")
    for i in range(0, 21):
        bin.append(0)
    cursor = cnn.cursor()
    select_stmt = "SELECT flicker_pst_phase_a, flicker_pst_phase_b, flicker_pst_phase_c FROM continuous_flicker_pst WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    while row is not None:    #get the bin label
        if row[0] != "NULL" and row[0] is not None:
            pst_a = float(row[0])
            i = bin_flicker(pst_a)
            bin[i] = bin[i] + 1
        if row[1] != "NULL" and row[1] is not None:
            pst_b = float(row[1])
            i = bin_flicker(pst_b)
            bin[i] = bin[i] + 1
        if row[2] != "NULL" and row[2] is not None:
            pst_c = float(row[2])
            i = bin_flicker(pst_c)
            bin[i] = bin[i] + 1
        row = cursor.fetchone()
    for i in range(0, 21):
        histogram_type = " Histogram PST"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO histogram (result_set_id, site_id, histogram_type, bin_id, bin_label, frequency) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(histogram_type)s, %(bin_id)s, %(bin_label)s, " \
                      "%(frequency)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp,
                                             'histogram_type': histogram_type, 'bin_id': i+1,
                                             'bin_label': label[i], 'frequency': float(bin[i])})
    result_cnn.commit()
    another_cursor.close()
    return bin

#histogram for long term flicker
def histogram_PLT(siteTmp, result_set_id):        
    bin = []     # a list stores the number of Short term flickers in that bin
    label = []
    label.append("< 0.1")
    label.append("0.1-0.2")
    label.append("0.2-0.3")
    label.append("0.3-0.4")
    label.append("0.4-0.5")
    label.append("0.5-0.6")
    label.append("0.6-0.7")
    label.append("0.7-0.8")
    label.append("0.8-0.9")
    label.append("0.9-1.0")
    label.append("1.0-1.1")
    label.append("1.1-1.2")
    label.append("1.2-1.3")
    label.append("1.3-1.4")
    label.append("1.4-1.5")
    label.append("1.5-1.6")
    label.append("1.6-1.7")
    label.append("1.7-1.8")
    label.append("1.8-1.9")
    label.append("1.9-2.0")
    label.append("> 2.0")
    for i in range(0, 21):
        bin.append(0)
    cursor = cnn.cursor()
    select_stmt = "SELECT flicker_plt_phase_a, flicker_plt_phase_b, flicker_plt_phase_c FROM continuous_flicker_plt WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    while row is not None:     #get the bin label
        if row[0] != "NULL" and row[0] is not None:
            plt_a = float(row[0])
            i = bin_flicker(plt_a)
            bin[i] = bin[i] + 1
        if row[1] != "NULL" and row[1] is not None:
            plt_b = float(row[1])
            i = bin_flicker(plt_b)
            bin[i] = bin[i] + 1
        if row[2] != "NULL" and row[2] is not None:
            plt_c = float(row[2])
            i = bin_flicker(plt_c)
            bin[i] = bin[i] + 1
        row = cursor.fetchone()
    for i in range(0, 21):
        histogram_type = " Histogram PLT"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO histogram (result_set_id, site_id, histogram_type, bin_id, bin_label, frequency) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(histogram_type)s, %(bin_id)s, %(bin_label)s, " \
                      "%(frequency)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp,
                                             'histogram_type': histogram_type, 'bin_id': i + 1,
                                             'bin_label': label[i], 'frequency': float(bin[i])})
    result_cnn.commit()
    another_cursor.close()
    return bin

#different bin label for THD, return the bin label
def bin_THD(thd):
    if thd < 0.25:
        k = 0
    if thd >= 0.25 and thd < 0.5:
        k = 1
    if thd >= 0.5 and thd < 0.75:
        k = 2
    if thd >= 0.75 and thd< 1:
        k = 3
    if thd >= 1 and thd < 1.25:
        k = 4
    if thd >= 1.25 and thd < 1.5:
        k = 5
    if thd >= 1.5 and thd < 1.75:
        k = 6
    if thd >= 1.75 and thd < 2:
        k = 7
    if thd >= 2 and thd < 2.25:
        k = 8
    if thd >= 2.25 and thd < 2.5:
        k = 9
    if thd >= 2.5 and thd < 2.75:
        k = 10
    if thd >= 2.75 and thd < 3:
        k = 11
    if thd >= 3 and thd < 3.25:
        k = 12
    if thd >= 3.25 and thd < 3.5:
        k = 13
    if thd >= 3.5 and thd < 3.75:
        k = 14
    if thd >= 3.75 and thd < 4:
        k = 15
    if thd >= 4 and thd < 4.25:
        k = 16
    if thd >= 4.25 and thd < 4.5:
        k = 17
    if thd >= 4.5 and thd < 4.75:
        k = 18
    if thd >= 4.75 and thd < 5:
        k = 19
    if thd >= 5 and thd < 5.25:
        k = 20
    if thd >= 5.25 and thd < 5.5:
        k = 21
    if thd >= 5.5 and thd < 5.75:
        k = 22
    if thd >= 5.75 and thd < 6:
        k = 23
    if thd >= 6 and thd < 6.25:
        k = 24
    if thd >= 6.25 and thd < 6.5:
        k = 25
    if thd >= 6.5 and thd < 6.75:
        k = 26
    if thd >= 6.75 and thd < 7:
        k = 27
    if thd >= 7 and thd < 7.25:
        k = 28
    if thd >= 7.25 and thd < 7.5:
        k = 29
    if thd >= 7.5 and thd < 7.75:
        k = 30
    if thd >= 7.75 and thd < 8:
        k = 31
    if thd >= 8:
        k = 32
    return k

#histogram of the THD
def histogram_THD(siteTmp, result_set_id):
    bin = []                     # a list stores the number of THD in that bin
    label = []
    label.append("< 0.25")
    label.append("0.5")
    label.append("0.75")
    label.append("1")
    label.append("1.25")
    label.append("1.5")
    label.append("1.75")
    label.append("2")
    label.append("2.25")
    label.append("2.5")
    label.append("2.75")
    label.append("3")
    label.append("3.25")
    label.append("3.5")
    label.append("3.75")
    label.append("4")
    label.append("4.25")
    label.append("4.5")
    label.append("4.75")
    label.append("5")
    label.append("5.25")
    label.append("5.5")
    label.append("5.75")
    label.append("6")
    label.append("6.25")
    label.append("6.5")
    label.append("6.75")
    label.append("7")
    label.append("7.25")
    label.append("7.5")
    label.append("7.75")
    label.append("8.0")
    label.append("> 8.0")
    for i in range(0, 33):
        bin.append(0)
    cursor = cnn.cursor()
    thd = []
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    reference = row[0]
    select_stmt = "SELECT thd__mean_phase_a, thd__mean_phase_b, thd__mean_phase_c, h1__mean_phase_a, h1__mean_phase_b, " \
                  "h1__mean_phase_c FROM continuous_harmonics WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    while row is not None:           #store all the thd value first
        if row[0]!="NULL" and row[0] is not None:
            thd_a = float(row[0])
            if row[3] != "NULL" and row[3] is not None:                         # has h1
                h1_a = float(row[3])
                newThd_a = thd_a * (h1_a/100 * reference) / reference
                thd.append(newThd_a)
            else:                                       # doesn't have h1
                thd.append(thd_a)
        if row[1]!="NULL" and row[1] is not None:
            thd_b = float(row[1])
            if row[4] != "NULL" and row[4] is not None:
                h1_b = float(row[4])
                newThd_b = thd_b * (h1_b/100 * reference) / reference
                thd.append(newThd_b)
            else:                                      # doesn't have h1
                thd.append(thd_b)
        if row[2]!="NULL" and row[2] is not None:
            thd_c = float(row[2])
            if row[5]!="NULL" and row[5] is not None:
                h1_c = float(row[5])
                newThd_c = thd_c * (h1_c/100 * reference) / reference
                thd.append(newThd_c)
            else:                                      # doesn't have h1
                thd.append(thd_c)
        row = cursor.fetchone()
    if thd:
        filtered = filterHarmonic_percentage(thd)
        if filtered: 
            for value in filtered:                        #get the bin label
                i = bin_THD(value)
                bin[i] = bin[i]+1
            for i in range(0, 33):
                histogram_type = "Histogram THD"
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO histogram (result_set_id, site_id, histogram_type, bin_id, bin_label, frequency) " \
                              "VALUES (%(result_set_id)s, %(site_id)s, %(histogram_type)s, %(bin_id)s, %(bin_label)s, " \
                              "%(frequency)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp,
                                                     'histogram_type': histogram_type, 'bin_id': i,
                                                     'bin_label': label[i], 'frequency': float(bin[i])})
            result_cnn.commit()
            another_cursor.close()
    return bin

#different bin label for voltage, return the bin lable
def bin_voltage(voltage):    # unit of voltage should be pu
    if voltage < 0.9:
        i = 0
    if voltage >= 0.9 and voltage < 0.91:
        i = 1
    if voltage >= 0.91 and voltage < 0.92:
        i = 2
    if voltage >= 0.92 and voltage < 0.93:
        i = 3
    if voltage >= 0.93 and voltage < 0.94:
        i = 4
    if voltage >= 0.94 and voltage < 0.95:
        i = 5
    if voltage >= 0.95 and voltage < 0.96:
        i = 6
    if voltage >= 0.96 and voltage < 0.97:
        i = 7
    if voltage >= 0.97 and voltage < 0.98:
        i = 8
    if voltage >= 0.98 and voltage < 0.99:
        i = 9
    if voltage >= 0.99 and voltage < 1:
        i = 10
    if voltage >= 1 and voltage < 1.01:
        i = 11
    if voltage >= 1.01 and voltage < 1.02:
        i = 12
    if voltage >= 1.02 and voltage < 1.03:
        i = 13
    if voltage >= 1.03 and voltage < 1.04:
        i = 14
    if voltage >= 1.04 and voltage < 1.05:
        i = 15
    if voltage >= 1.05 and voltage < 1.06:
        i = 16
    if voltage >= 1.06 and voltage < 1.07:
        i = 17
    if voltage >= 1.07 and voltage < 1.08:
        i = 18
    if voltage >= 1.08 and voltage < 1.09:
        i = 19
    if voltage >= 1.09 and voltage < 1.1:
        i =20
    if voltage >= 1.1 and voltage < 1.11:
        i = 21
    if voltage >= 1.11 and voltage < 1.12:
        i = 22
    if voltage >= 1.12 and voltage < 1.13:
        i = 23
    if voltage >= 1.13 and voltage < 1.14:
        i = 24
    if voltage >= 1.14 and voltage < 1.15:
        i = 25
    if voltage >= 1.15:
        i = 26
    return i

#histogram for voltage 
def histogram_voltage(siteTmp, result_set_id):
    bin = []                      # a list stores the number of voltage in that bin
    tmp = []
    label = []
    label.append("< 0.9")
    label.append("0.9-0.91")
    label.append("0.91-0.92")
    label.append("0.92-0.93")
    label.append("0.93-0.94")
    label.append("0.94-0.95")
    label.append("0.95-0.96")
    label.append("0.95-0.96")
    label.append("0.95-0.96")
    label.append("0.96-0.97")
    label.append("0.97-0.98")
    label.append("0.98-0.99")
    label.append("0.99-1.0")
    label.append("1.0-1.01")
    label.append("1.01-1.02")
    label.append("1.02-1.03")
    label.append("1.03-1.04")
    label.append("1.05-1.06")
    label.append("1.06-1.07")
    label.append("1.07-1.08")
    label.append("1.08-1.09")
    label.append("1.09-1.10")
    label.append("1.10-1.11")
    label.append("1.11-1.12")
    label.append("1.12-1.13")
    label.append("1.13-1.14")
    label.append("1.14-1.15")
    label.append("> 1.15")
    for i in range(0, 27):
        bin.append(0)
    cursor = cnn. cursor()
    select_stmt = "SELECT voltage_mean_phase_a, voltage_mean_phase_b, voltage_mean_phase_c FROM continuous_voltage " \
                  "WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    while row is not None:            #store all the data first
        if row[0] != "NULL" and row[0] is not None:
            Va = float(row[0])
            tmp.append(Va)
        if row[1] != "NULL" and row[1] is not None:
            Vb = float(row[1])
            tmp.append(Vb)
        if row[2] != "NULL" and row[2] is not None:
            Vc = float(row[2])
            tmp.append(Vc)
        row = cursor.fetchone()
    volt_voltage = cvolt2Volt(tmp, siteTmp)                  #normalization in order to filter
    filtered_voltage = filterVoltage(volt_voltage, siteTmp)
    if filtered_voltage:
        pu_voltage = volt2Pu(filtered_voltage, siteTmp)           #convert to pu
        for voltage in pu_voltage:              #get the bin label
            i = bin_voltage(voltage)
            bin[i] = bin[i]+1
        for i in range(0, 27):
            histogram_type = "Histogram voltage"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO histogram (result_set_id, site_id, histogram_type, bin_id, bin_label, frequency) " \
                          "VALUES (%(result_set_id)s, %(site_id)s, %(histogram_type)s, %(bin_id)s, %(bin_label)s, " \
                          "%(frequency)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp,
                                                 'histogram_type': histogram_type, 'bin_id': i,
                                                 'bin_label': label[i], 'frequency': float(bin[i])})
        result_cnn.commit()
        another_cursor.close()
    return bin
    
#different bin label for unbalance, return bin label
def bin_unbalance(unbalance):
    if unbalance < 0.2:
        i = 0
    if unbalance >= 0.2 and unbalance < 0.4:
        i = 1
    if unbalance >= 0.4 and unbalance < 0.6:
        i = 2
    if unbalance >= 0.6 and unbalance < 0.8:
        i = 3
    if unbalance >= 0.8 and unbalance < 1.0:
        i = 4
    if unbalance >= 1.0 and unbalance < 1.2:
        i = 5
    if unbalance >= 1.2 and unbalance < 1.4:
        i = 6
    if unbalance >= 1.4 and unbalance < 1.6:
        i = 7
    if unbalance >= 1.6 and unbalance < 1.8:
        i = 8
    if unbalance >= 1.8 and unbalance < 2.0:
        i = 9
    if unbalance >= 2.0 and unbalance < 2.2:
        i = 10
    if unbalance >= 2.2 and unbalance < 2.4:
        i = 11
    if unbalance >= 2.4 and unbalance < 2.6:
        i = 12
    if unbalance >= 2.6 and unbalance < 2.8:
        i = 13
    if unbalance >= 2.8 and unbalance < 3.0:
        i = 14
    if unbalance >= 3.0 and unbalance < 3.2:
        i = 15
    if unbalance >= 3.2 and unbalance < 3.4:
        i = 16
    if unbalance >= 3.4 and unbalance < 3.6:
        i = 17
    if unbalance >= 3.6 and unbalance < 3.8:
        i = 18
    if unbalance >= 3.8 and unbalance < 4.0:
        i = 19
    if unbalance >= 4.0 and unbalance < 4.2:
        i = 20
    if unbalance >= 4.2 and unbalance < 4.4:
        i = 21
    if unbalance >= 4.4 and unbalance < 4.6:
        i = 22
    if unbalance >= 4.6 and unbalance < 4.8:
        i = 23
    if unbalance >= 4.8 and unbalance < 5.0:
        i = 24
    if unbalance >= 5.0 and unbalance < 5.2:
        i = 25
    if unbalance >= 5.2 and unbalance < 5.4:
        i = 26
    if unbalance >= 5.4 and unbalance < 5.6:
        i = 27
    if unbalance >= 5.6 and unbalance < 5.8:
        i = 28
    if unbalance >= 5.8 and unbalance < 6.0:
        i = 29
    if unbalance >= 6:
        i = 30
    return i

#histogram for unbalance
def histogram_unbalance(siteTmp, result_set_id):
    bin = []                        #store the number of unbalanace in that bin
    label = []
    strength = "Weak"
    label.append("< 0.2")
    label.append("0.2-0.4")
    label.append("0.4-0.6")
    label.append("0.6-0.8")
    label.append("0.8-1.0")
    label.append("1.0-1.2")
    label.append("1.2-1.4")
    label.append("1.4-1.6")
    label.append("1.6-1.8")
    label.append("1.8-2.0")
    label.append("2.0-2.2")
    label.append("2.2-2.4")
    label.append("2.4-2.6")
    label.append("2.6-2.8")
    label.append("2.8-3.0")
    label.append("3.0-3.2")
    label.append("3.2-3.4")
    label.append("3.4-3.6")
    label.append("3.6-3.8")
    label.append("3.8-4.0")
    label.append("4.0-4.2")
    label.append("4.2-4.4")
    label.append("4.4-4.6")
    label.append("4.6-4.8")
    label.append("4.8-5.0")
    label.append("5.0-5.2")
    label.append("5.2-5.4")
    label.append("5.4-5.6")
    label.append("5.6-5.8")
    label.append("5.8-6.0")
    label.append("> 6.0")
    for i in range(0, 31):
        bin.append(0)
    cursor = cnn.cursor()
    select_stmt = "SELECT strength From site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        strength = row[0]
    tmp_unbalance = []  # need to be normalised
    normalized_unbalance = []
    startDate = "2015-07-01 00:00:00"
    endDate = "2016-06-30 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    if strength == "Strong":
        select_stmt = "SELECT unbalance_mean FROM continuous_voltage WHERE site_id = %(site_id)s" \
                      "AND time_stamp >= %(start)s AND time_stamp < %(end)s AND unbalance_mean <> 'NULL'"
        # for unbalance_mean is not NULL
        cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
        row = cursor.fetchone()
        while row is not None:  # have measured unbalanace data
            unbalance = float(row[0])
            tmp_unbalance.append(unbalance)
            row = cursor.fetchone()
        tmp_vuf = siteUnbalance(siteTmp, startDate, endDate)  # calculate vuf, only need to filter
        if normalized_unbalance:
            normalized_unbalance = cvolt2Volt(tmp_unbalance, siteTmp)
        normalized_unbalance.extend(tmp_vuf)  # put two normalized lists together
        if normalized_unbalance:
            filtered_unbalance = filterUnbalance(normalized_unbalance)
            if filtered_unbalance:
                for value in filtered_unbalance:
                    i = bin_unbalance(value)    #get bin label
                    bin[i] = bin[i]+1
                for i in range(0, 31):
                    histogram_type = "Histogram unbalance"
                    another_cursor = result_cnn.cursor()
                    nsert_stmt = "INSERT INTO histogram (result_set_id, site_id, histogram_type, bin_id, bin_label, frequency) " \
                                  "VALUES (%(result_set_id)s, %(site_id)s, %(histogram_type)s, %(bin_id)s, %(bin_label)s, " \
                                  "%(frequency)s)"
                    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp,
                                                     'histogram_type': histogram_type, 'bin_id': i,
                                                     'bin_label': label[i], 'frequency': float(bin[i])})
                result_cnn.commit()
                another_cursor.close()
    return bin