from database_connections import *
import sys
import mysql.connector
from mysql.connector import errorcode
import numpy as np
import math
import datetime
from normalization import *
from filter import *

#calcultage the site coverage 
def siteCoverage(siteTmp, startDate, endDate, result_set_id):
    #determine the time interval, then calculate how many intervals in the whole year - potential record
    #in the database, count how many intervals - actual record
    cursor = cnn.cursor()
    siteCover = 0
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    # find the time interval
    select_stmt = "SELECT time_stamp FROM continuous_voltage WHERE site_id = %(site_id)s AND " \
                  "time_stamp >= %(sDate)s AND time_stamp < %(eDate)s LIMIT 0, 4"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'sDate': startDate, 'eDate': endDate})
    rows = cursor.fetchall()
    time = []
    for row in rows:
        time.append(row[0])
    if time:
        interval = (time[1] - time[0]).seconds
        interval = interval//60
        i = 2
        while interval > 60 and i <= 4:     #keep the time interval less than 1 hour
            interval = (time[i] - time[i-1]).seconds
            interval = interval // 60
            i = i+1
        startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
        endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
        totallength = (endDate - startDate).days
        totallength = totallength *24 * 60     #how many minutes between starttime and endtime
        potential = totallength / interval     #this is the potential record
        actual = 0
        select_stmt = "SELECT COUNT(site_id) FROM continuous_voltage WHERE site_id = %(site_id)s AND " \
                      "time_stamp >= %(sDate)s AND time_stamp < %(eDate)s"
        cursor.execute(select_stmt, {'site_id': siteTmp, 'sDate': startDate, 'eDate': endDate})
        row = cursor.fetchone()
        if row is not None:
            actual = row[0]
        siteCover = actual / potential
        cnn.commit()
        cursor.close()
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month ==  3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "Coverage"
        cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': siteCover})
        result_cnn.commit()
        cursor.close()
    return siteCover

#calculate the V99                                                                                                                                                                                               
def V99(siteTmp, startDate, endDate, result_set_id):    #voltage values in pu
    cursor = cnn.cursor()
    tmp_voltage = []   # store the voltage which is from database for later use
    voltageA = []
    voltageB = []
    voltageC = []
    ninetynine = []
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT voltage_mean_phase_a FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    while rows is not None:   #store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  #string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)   # normalize voltage to volts for later filtering
        voltageA = filterVoltage(normalized_voltage, siteTmp)   # get the filtered voltage value
        if voltageA:
            voltageA = volt2Pu(voltageA, siteTmp)     #after filter, still have value in the list
            if voltageA:
                ninetynine.append(np.percentile(voltageA, 99))

    select_stmt = "SELECT voltage_mean_phase_b FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []   # clear the value in tmp_voltage
    while rows is not None:   #store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  #string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    if tmp_voltage:    #has value inside
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)   # normalize voltage to volts for later filtering
        voltageB = filterVoltage(normalized_voltage, siteTmp)   # get the filtered voltage value
        if voltageB:
            voltageB = volt2Pu(voltageB, siteTmp)               #after filter, still have value in the list
            if voltageB:
                ninetynine.append(np.percentile(voltageB, 99))

    select_stmt = "SELECT voltage_mean_phase_c FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageC = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageC:
            voltageC = volt2Pu(voltageC, siteTmp)
            if voltageC:
                ninetynine.append(np.percentile(voltageC, 99))

    if ninetynine:
        ninetyninePercentile = max(ninetynine)
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "V99"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(ninetyninePercentile)})
        result_cnn.commit()
        another_cursor.close()
    else:
        ninetyninePercentile = "NULL"
    del voltageA, voltageB, voltageC
    cnn.commit()
    cursor.close()
    return ninetyninePercentile

#calculate V1
def V1(siteTmp, startDate, endDate, result_set_id):  # VOLTAGE VALUES IN pu     
    cursor = cnn.cursor()
    tmp_voltage = []  # store the voltage which is from database for later use
    voltageA = []
    voltageB = []
    voltageC = []
    first = []
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT voltage_mean_phase_a FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageA = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageA:
            voltageA = volt2Pu(voltageA, siteTmp)
            if voltageA:
                first.append(np.percentile(voltageA, 1))

    select_stmt = "SELECT voltage_mean_phase_b FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageB = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageB:
            voltageB = volt2Pu(voltageB, siteTmp)
            if voltageB:
                first.append(np.percentile(voltageB, 1))

    select_stmt = "SELECT voltage_mean_phase_c FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageC = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageC:
            voltageC = volt2Pu(voltageC, siteTmp)
            if voltageC:
                first.append(np.percentile(voltageC, 1))

    if first:
        firstPercentile = min(first)
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "V1"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(firstPercentile)})
        result_cnn.commit()
        another_cursor.close()
    else:
        firstPercentile = "NULL"
    del voltageA, voltageB, voltageC
    cnn.commit()
    cursor.close()
    return firstPercentile

#calculate site spread
def VSpread(siteTmp, startDate, endDate, result_set_id):   
    cursor = cnn.cursor()
    difference = []
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    # for phase a
    select_stmt = "SELECT voltage_mean_phase_a FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    voltageA = []
    voltageB = []
    voltageC = []
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageA = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageA:     #after filter, still have value in the list
            voltageA = volt2Pu(voltageA, siteTmp)
            if voltageA:
                ninetynineA = np.percentile(voltageA, 99)
                firstA = np.percentile(voltageA, 1)
                difference.append(ninetynineA - firstA)

    # for phase b
    select_stmt = "SELECT voltage_mean_phase_b FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageB = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageB:
            voltageB = volt2Pu(voltageB, siteTmp)
            if voltageB:
                ninetynineB = np.percentile(voltageB, 99)
                firstB = np.percentile(voltageB, 1)
                difference.append(ninetynineB - firstB)

    # for phase c
    select_stmt = "SELECT voltage_mean_phase_c FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    tmp_voltage = []  # clear the value in tmp_voltage
    while rows is not None:  # store the voltages which have values
        voltage = rows[0]
        if voltage != "NULL" and voltage is not None:
            voltage = float(voltage)  # string type converts to float type
            tmp_voltage.append(voltage)
        rows = cursor.fetchone()
    cnn.commit()
    if tmp_voltage:
        normalized_voltage = cvolt2Volt(tmp_voltage, siteTmp)  # normalize voltage to volts for later filtering
        voltageC = filterVoltage(normalized_voltage, siteTmp)  # get the filtered voltage value
        if voltageC:
            voltageC = volt2Pu(voltageC, siteTmp)
            if voltageC:
                ninetynineC = np.percentile(voltageC, 99)
                firstC = np.percentile(voltageC, 1)
                difference.append(ninetynineC - firstC)

    if difference:
        vSpread = max(difference) * 100
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "Spread"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(vSpread)})
        result_cnn.commit()
        another_cursor.close()
    else:
        vSpread = "NULL"
    del difference, voltageA, voltageB, voltageC
    cnn.commit()
    cursor.close()
    return vSpread

#calculate site AVDI
def siteAVDI(siteTmp, startDate, endDate, result_set_id):    #only for LV site, by voltage 
    cursor = cnn.cursor()
    select_stmt = "SELECT mor_voltage, reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    mor = row[0]
    ref = row[1]
    tmp_voltageA = []
    tmp_voltageB = []
    tmp_voltageC = []
    AVDA = []
    AVDB = []       #store the data which fetched from database
    AVDC = []
    ninetynine = []
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT voltage_mean_phase_a, voltage_mean_phase_b, voltage_mean_phase_c FROM continuous_voltage WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    while rows is not None:
        if rows[0] != "NULL" and rows[0] is not None:  #convert to float type
            voltageA = float(rows[0])
            tmp_voltageA.append(voltageA)
        if rows[1] != "NULL" and rows[1] is not None:  #convert to float type
            voltageB = float(rows[1])
            tmp_voltageB.append(voltageB)
        if rows[2] != "NULL" and rows[2] is not None:  #convert to float type
            voltageC = float(rows[2])
            tmp_voltageC.append(voltageC)   # store all the data first
        rows = cursor.fetchone()
    if tmp_voltageA:
        normalized_voltageA = cvolt2Volt(tmp_voltageA, siteTmp)  # normalize voltage to volts for later filtering
        filtered_voltageA = filterVoltage(normalized_voltageA, siteTmp)  # filter all the voltage
        for voltage in filtered_voltageA:
            AVDA.append(abs(voltage - mor) / ref)
    if tmp_voltageB:
        normalized_voltageB = cvolt2Volt(tmp_voltageB, siteTmp)  # normalize voltage to volts for later filtering
        filtered_voltageB = filterVoltage(normalized_voltageB, siteTmp)
        for voltage in filtered_voltageB:
            AVDB.append(abs(voltage - mor) / ref)
    if tmp_voltageC:
        normalized_voltageC = cvolt2Volt(tmp_voltageC, siteTmp)  # normalize voltage to volts for later filtering
        filtered_voltageC = filterVoltage(normalized_voltageC, siteTmp)
        for voltage in filtered_voltageC:
            AVDC.append(abs(voltage - mor) / ref)
    if AVDA:    #has values for phase a
        ninetynine.append(np.percentile(AVDA, 99))
    if AVDB:     #has values for phase b
        ninetynine.append(np.percentile(AVDB, 99))
    if AVDC:     #has values for phase c
        ninetynine.append(np.percentile(AVDC, 99))
    if ninetynine:    #has 99th percentile value, then insert into table
        AVDI = max(ninetynine) * 100
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "AVDI"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                            'period': period, 'value': float(AVDI)})
        result_cnn.commit()
        another_cursor.close()
    else:
        AVDI = "NULL"
    cnn.commit()
    cursor.close()
    return AVDI

#calculate site vuf value if no measured unbalance value in table
def siteUnbalance(siteTmp, startDate, endDate):
    #if no measured unbalance
    #if site is strong site
    all_vuf = []
    cursor = cnn.cursor()
    select_stmt = "SELECT voltage_mean_phase_a, voltage_mean_phase_b, voltage_mean_phase_c FROM continuous_voltage " \
                  "WHERE site_id = %(site_id)s AND time_stamp >= %(start)s AND time_stamp < %(end)s " \
                  "AND unbalance_mean = 'NULL'"   #for unbalance_mean is NULL
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    row = cursor.fetchone()
    voltage_tmp = []       #a list of list
    while row is not None:     #store all the data first
        tmp = []
        Va = row[0]
        if Va != "NULL" and Va is not None:
            Va = float(Va)
            tmp.append(Va)
        Vb = row[1]
        if Vb != "NULL" and Vb is not None:
            Vb = float(Vb)
            tmp.append(Vb)
        Vc = row[2]
        if Vc != "NULL" and Vc is not None:
            Vc = float(Vc)
            tmp.append(Vc)
        voltage_tmp.append(tmp)
        row = cursor.fetchone()
    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    select_stmt = "SELECT continuous_scaling FROM instrument WHERE instrument_id = %(instrument_id)s"
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()  # continous scaling factor from instrument database
    if row is not None:
        cscalFactor = row[0]
    else:
        cscalFactor = 1
    select_stmt = "SELECT mor_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    mor = row[0]  # mor is the site middle of the range voltage value from site database
    for i in range(0, len(voltage_tmp)):    #calculate vuf list by list
        filtered_Tmp = []
        for j in range(0, len(voltage_tmp[i])):
            normalized = voltage_tmp[i][j] * cscalFactor  #normalization
            if normalized / mor <= 1.2 and normalized / mor >= 0.8:   #filter
                filtered_Tmp.append(normalized)
        dividend = 0
        divisor = 0    #dividend and divisor are used to calculate beta
        if len(filtered_Tmp) > 1:   # make sure not only one phase has value
            pu_tmp = volt2Pu(filtered_Tmp, siteTmp)      #normalized voltages convert to pu
            for voltage in pu_tmp:
                dividend = dividend + (pow(voltage, 4))
                divisor = divisor + (pow(voltage, 2))
            beta = dividend/pow(divisor, 2)
            if (3 - 6 * beta) >= 0:        #make sure no math domain error
                if (1 - math.sqrt(3 - 6 * beta)) >= 0:
                    vuf = math.sqrt((1 - math.sqrt(3 - 6 * beta)) / (1 + math.sqrt(3 - 6 * beta)))
                    all_vuf.append(vuf)
    cnn.commit()
    cursor.close()
    return all_vuf

#calculate site VUFI
def VUFI(siteTmp, startDate, endDate, result_set_id): 
    cursor = cnn.cursor()
    strength = "Weak"
    select_stmt = "SELECT strength From site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        strength = row[0]
    VUFIndex = "NULL"
    tmp_unbalance = []                         #need to be normalised
    normalized_unbalance = []              #store normalized unbalance value
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    if strength == "Strong":
        select_stmt = "SELECT unbalance_mean FROM continuous_voltage WHERE site_id = %(site_id)s" \
                      "AND time_stamp >= %(start)s AND time_stamp < %(end)s AND unbalance_mean <> 'NULL'"
                      #for unbalance_mean is not NULL
        cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
        row = cursor.fetchone()
        while row is not None:             #have measured unbalanace data
            unbalance = float(row[0])
            tmp_unbalance.append(unbalance)
            row = cursor.fetchone()
        tmp_vuf = siteUnbalance(siteTmp, startDate, endDate)    #calculate vuf, only need to filter
        if tmp_unbalance:            #if there has value for measured unbalance data
            normalized_unbalance = cvolt2Volt(tmp_unbalance, siteTmp)
        normalized_unbalance.extend(tmp_vuf)                      #put two normalized lists together
        if normalized_unbalance:              #if this site has value to calculate VUFI
            filtered_unbalance = filterUnbalance(normalized_unbalance)
            if filtered_unbalance:
                VUFIndex = np.percentile(filtered_unbalance, 95)
                if (endDate - startDate).days > 31:
                    period = "Annual"
                else:
                    month = startDate.month
                    if month == 1:
                        period = "January"
                    if month == 2:
                        period = "February"
                    if month == 3:
                        period = "March"
                    if month == 4:
                        period = "April"
                    if month == 5:
                        period = "May"
                    if month == 6:
                        period = "June"
                    if month == 7:
                        period = "July"
                    if month == 8:
                        period = "August"
                    if month == 9:
                        period = "September"
                    if month == 10:
                        period = "October"
                    if month == 11:
                        period = "November"
                    if month == 12:
                        period = "December"
                index = "VUFI"
                another_cursor = result_cnn.cursor()
                insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                              "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
                another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(VUFIndex)})
                result_cnn.commit()
                another_cursor.close()
    cnn.commit()
    cursor.close()
    return VUFIndex

#calculate site THDI 
def THDI(siteTmp, startDate, endDate, result_set_id):   
    cursor = cnn.cursor()
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    reference = row[0]
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT thd__mean_phase_a, h1__mean_phase_a FROM continuous_harmonics WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    thdList = []
    thdResult = []
    while rows is not None:          #store all the data first
        if rows[0]!="NULL" and rows[0] is not None:
            thd = float(rows[0])
            tmp_h1 = rows[1]         #h1 may be null value, null cannot change to float
            if tmp_h1 != "NULL" and tmp_h1 is not None:
                h1 = float(tmp_h1)   #change h1 to float type
                newThd = thd * (h1/100 * reference) / reference
                thdList.append(newThd)
            else:
                thdList.append(thd)
        rows = cursor.fetchone()
    if thdList:
        filtered = filterHarmonic_percentage(thdList)
        if filtered:
            thdResult.append(np.percentile(filtered, 95))

    select_stmt = "SELECT thd__mean_phase_b, h1__mean_phase_b FROM continuous_harmonics WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    thdList = []
    while rows is not None:      #store all the data first
        if rows[0]!="NULL" and rows[0] is not None:
            thd = float(rows[0])
            tmp_h1 = rows[1]              # h1 may be null value, null cannot change to float
            if tmp_h1 != "NULL" and tmp_h1 is not None:
                h1 = float(tmp_h1)        # change h1 to float type
                newThd = thd * h1/100 * reference / reference
                thdList.append(newThd)
            else:
                thdList.append(thd)
        rows = cursor.fetchone()
    if thdList:
        filtered = filterHarmonic_percentage(thdList)
        if filtered:
            thdResult.append(np.percentile(filtered, 95))

    select_stmt = "SELECT thd__mean_phase_c, h1__mean_phase_c FROM continuous_harmonics WHERE site_id = %(site_id)s" \
                  "AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    rows = cursor.fetchone()
    thdList = []
    while rows is not None:                 #store all the data first
        if rows[0] != "NULL" and rows[0] is not None:
            thd = float(rows[0])
            tmp_h1 = rows[1]            # h1 may be null value, null cannot change to float
            if tmp_h1 != "NULL" and tmp_h1 is not None:
                h1 = float(tmp_h1)       # change h1 to float type
                newThd = thd * h1/100 * reference / reference
                thdList.append(newThd)
            else:
                thdList.append(thd)
        rows = cursor.fetchone()
    if thdList:
        filtered = filterHarmonic_percentage(thdList)
        if filtered:
            thdResult.append(np.percentile(filtered, 95))

    if thdResult:
        THDIndex = max(thdResult)
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "THDI"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(THDIndex)})
        result_cnn.commit()
        another_cursor.close()
    else:
        THDIndex = "NULL"
    cnn.commit()
    cursor.close()
    return THDIndex

#calculate site HII
def HII(siteTmp, result_set_id):      #Annual
    HIIndex = "NULL"
    cursor = result_cnn.cursor()
    select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s" \
                  "AND site_id = %(site_id)s AND period = 'Annual' AND index_id = 'H5I'"
    cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        H5I = float(row[0])
        select_stmt = "SELECT value FROM site_result WHERE result_set_id = '2015-2016 Full Run'" \
                      "AND site_id = %(site_id)s AND period = 'Annual' AND index_id = 'THDI'"
        cursor.execute(select_stmt,{'site_id': siteTmp})
        row = cursor.fetchone()
        if row is not None:
            THDIndex = float(row[0])
            HIIndex = H5I/THDIndex * 100
            period = "Annual"
            index = "HII"
            another_cursor = result_cnn.cursor()
            insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                          "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
            another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                         'period': period, 'value': float(HIIndex)})
            result_cnn.commit()
            another_cursor.close()
    return HIIndex

#calculate site HnI from H2I to H25I
def HnI(siteTmp, n, result_set_id, reference, uom_harmonics):     #after calculation, the unit should be %
    cursor = cnn.cursor()
    ninetyfive = []
    tmp_harmonic_a = []
    percentage_a = []
    filtered_a = []
    tmp_harmonic_b = []
    percentage_b = []
    filtered_b = []
    tmp_harmonic_c = []
    percentage_c = []
    filtered_c =[]

    harmonicOrder = "h" + str(n) + "__mean_phase_a"
    cursor.execute("SELECT %s FROM continuous_harmonics WHERE site_id = '%s'" % (harmonicOrder, siteTmp))
    rows = cursor.fetchone()
    if uom_harmonics == "volts":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_a.append(harmonic)   #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_a) != 0:                      # some site may not have individual harmonic value
            normalized_a = cvolt2Volt(tmp_harmonic_a, siteTmp)
            for value in normalized_a:
                percentage_a.append(value/reference)
            filtered_a = filterHarmonic_percentage(percentage_a)
    if uom_harmonics == "%":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_a.append(harmonic)                #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_a) != 0:                           # some site may not have individual harmonic value
            normalized_a = percent2Volt(tmp_harmonic_a, siteTmp)
            for value in normalized_a:
                percentage_a.append(value/reference)
            filtered_a = filterHarmonic_percentage(percentage_a)
    if len(filtered_a) != 0:
        ninetyfive.append(np.percentile(filtered_a, 95))

    harmonicOrder = "h" + str(n) + "__mean_phase_b"
    cursor.execute("SELECT %s FROM continuous_harmonics WHERE site_id = '%s'" % (harmonicOrder, siteTmp))
    rows = cursor.fetchone()
    if uom_harmonics == "volts":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_b.append(harmonic)   #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_b) != 0:          # some site may not have individual harmonic value
            normalized_b = cvolt2Volt(tmp_harmonic_b, siteTmp)
            for value in normalized_b:
                percentage_b.append(value/reference)
            filtered_b = filterHarmonic_percentage(percentage_b)
    if uom_harmonics == "%":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_b.append(harmonic)   #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_b) != 0:   # some site may not have individual harmonic value
            normalized_b = percent2Volt(tmp_harmonic_b, siteTmp)
            for value in normalized_b:
                percentage_b.append(value / reference)
            filtered_b = filterHarmonic_percentage(percentage_b)
    if len(filtered_b)!= 0:
        ninetyfive.append(np.percentile(filtered_b, 95))

    harmonicOrder = "h" + str(n) + "__mean_phase_c"
    cursor.execute("SELECT %s FROM continuous_harmonics WHERE site_id = '%s'" % (harmonicOrder, siteTmp))
    rows = cursor.fetchone()
    if uom_harmonics == "volts":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_c.append(harmonic)   #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_c) != 0:          # some site may not have individual harmonic value
            normalized_c = cvolt2Volt(tmp_harmonic_c, siteTmp)
            for value in normalized_c:
                percentage_c.append(value/reference)
            filtered_c = filterHarmonic_percentage(percentage_c)
    if uom_harmonics == "%":
        while rows is not None:
            if rows[0]!='NULL' and rows[0] is not None:
                harmonic = float(rows[0])
                tmp_harmonic_c.append(harmonic)   #store all the data first
            rows = cursor.fetchone()
        if len(tmp_harmonic_c) != 0:   # some site may not have individual harmonic value
            normalized_c = percent2Volt(tmp_harmonic_c, siteTmp)
            for value in normalized_c:
                percentage_c.append(value / reference)
            filtered_c = filterHarmonic_percentage(percentage_c)
    if len(filtered_c)!= 0:
        ninetyfive.append(np.percentile(filtered_c, 95))

    if len(ninetyfive)!= 0:
        HnIndex = max(ninetyfive)
        period = "Annual"
        index = "H" + str(n) + "I"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(HnIndex)})
        result_cnn.commit()
        another_cursor.close()
    else:
        HnIndex = 0
    cnn.commit()
    cursor.close()
    return HnIndex

#caluclate all site HnI, call HnI function
def site_HnI(siteTmp, result_set_id):   
    cursor = cnn.cursor()
    select_stmt = "SELECT ripple_freq_1, ripple_freq_2, ripple_freq_3 FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row[0] is not None:
        fre_1 = float(row[0])
        exclude = fre_1/50
        exclude_1 = int(round(exclude))
    else:
        exclude_1 = 0
    if row[1] is not None:
        fre_2 = float(row[1])
        exclude = fre_2 / 50
        exclude_2 = int(round(exclude))
    else:
        exclude_2 = 0
    if row[2] is not None:
        fre_3 = float(row[2])
        exclude = fre_3 / 50
        exclude_3 = int(round(exclude))
    else:
        exclude_3 = 0

    instrumentTmp = siteTmp[:2] + 'U' + siteTmp[2:]
    select_stmt = "SELECT uom_harmonics FROM instrument WHERE instrument_id = %(instrument_id)s"  # get the uom of harmonics
    cursor.execute(select_stmt, {'instrument_id': instrumentTmp})
    row = cursor.fetchone()
    if row is not None:
        if row[0] != '':
            uom_harmonics = row[0]
            select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
            cursor.execute(select_stmt, {'site_id': siteTmp})
            row = cursor.fetchone()
            reference = row[0]

            for n in range(2, 26):
                if n != exclude_1 and n != exclude_2 and n != exclude_3:   #Check if the calculation of the index for this harmonic order is to be excluded
                    HnI(siteTmp, n, result_set_id, reference, uom_harmonics)
    return

def HCI(siteTmp, result_set_id):    #if cannot find the limit, use the default limit  Annual
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"  #find the utility for that site
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    utility = row[0]
    level = row[1]
    nominalVoltage = row[2]
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    percentage = []
    HnI_list = []
    n_list = []
    if level == "LV":
        for n in range(2, 26):     #Find HnI value which is calculated before
            index = 'H' + str(n) + 'I'
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s AND site_id = %(site_id)s " \
                          "AND period = 'Annual' AND index_id = %(index)s"
            cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index': index})
            row = cursor.fetchone()
            if row is not None:      #have value for HnI
                HnIndex = float(row[0])
                HnI_list.append(HnIndex)    #store all the HnI first
                n_list.append(n)
        for i in range(len(HnI_list)):
            select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
            index = "H" + str(n_list[i]) + "I"        #used for index name in sql query
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index, 'voltage_level': level})
            row = cursor.fetchall()
            if len(row) == 0:                            # get default limit
                default_juristiction = 1
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                for r in row:
                    defaule_limit = r[0]
                limit = float(defaule_limit)
            else:
                for r in row:
                    limit = float(r[0])
            percentage.append(HnI_list[i]/limit)
    if level == "MV":
        for n in range(2, 26):   #Find HnI value which is calculated before
            index = 'H' + str(n) + 'I'
            select_stmt = "SELECT value FROM site_result WHERE result_set_id = %(result_set_id)s AND site_id = %(site_id)s " \
                          "AND period = 'Annual' AND index_id = %(index)s"
            cursor.execute(select_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index': index})
            row = cursor.fetchone()
            if row is not None:      #have value for HnI
                HnIndex = float(row[0])
                HnI_list.append(HnIndex)    #store all the HnI first
                n_list.append(n)
        for i in range(len(HnI_list)):
            limit = []  # used for store limit
            index = "H" + str(n_list[i]) + "I"
            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                          "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                          "voltage_level = %(voltage_level)s"
            cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                         'nominal_voltage': nominalVoltage,
                                         'voltage_level': level})  # limit for this index
            row = cursor.fetchall()
            if len(row) == 0:  # find the limit for nominal voltage is NULL
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage is NULL and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': index,
                                             'voltage_level': level})
                row = cursor.fetchall()
                if len(row) == 0:  # find the default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt,
                                   {'jurisdiction_id': default_juristiction, 'index_name': index,
                                    'nominal_voltage': nominalVoltage, 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit for nominal voltage is NULL
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': index,
                                        'voltage_level': level})
                        row = cursor.fetchall()
                        for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                            default_limit = float(r[0])
                        HnIlimit = default_limit
                    else:  # has limit for default juristiction id and nominal voltage is not NULL
                        for r in row:
                            limit.append(float(r[0]))
                        i = len(limit)
                        HnIlimit = limit[i - 1]
                else:  # has limit for this juristiction id and nominal voltage is NULL
                    for r in row:
                        default_limit = float(r[0])
                        HnIlimit = default_limit
            else:  # has limit for this juristiction id and nominal voltage is not NULL
                for r in row:
                    limit.append(float(r[0]))
                i = len(limit)
                HnIlimit = limit[i - 1]
                # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
            percentage.append(HnIndex/HnIlimit)
    if percentage:
        HCIndex = max(percentage) * 100
        period = "Annual"
        index = "HCI"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(HCIndex)})
        result_cnn.commit()
        another_cursor.close()
    else:
        HCIndex = "NULL"
    del percentage
    cnn.commit()
    cursor.close()
    return HCIndex

#calculate site PSTI
def PSTI(siteTmp, startDate, endDate, result_set_id):   
    cursor = cnn.cursor()
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT flicker_pst_phase_a, flicker_pst_phase_b, flicker_pst_phase_c FROM continuous_flicker_pst " \
                  "WHERE site_id = %(site_id)s AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    row = cursor.fetchone()
    phaseA = []
    filtered_a = []
    phaseB = []
    filtered_b = []
    phaseC = []
    filtered_c = []
    ninetyfive = []
    while row is not None:  #store all the data first
        if row[0]!= "NULL" and row[0] is not None:
            phaseA.append(float(row[0]))
        if row[1] != "NULL" and row[1] is not None:
            phaseB.append(float(row[1]))
        if row[2] != "NULL" and row[2] is not None:
            phaseC.append(float(row[2]))
        row = cursor.fetchone()
    if phaseA:
        filtered_a = filterFlicker(phaseA)
    if phaseB:
        filtered_b = filterFlicker(phaseB)
    if phaseC:
        filtered_c = filterFlicker(phaseC)
    if filtered_a:
        ninetyfive.append(np.percentile(filtered_a, 95))
    if filtered_b:
        ninetyfive.append(np.percentile(filtered_b, 95))
    if filtered_c:
        ninetyfive.append(np.percentile(filtered_c, 95))
    if ninetyfive:
        PSTIndex = max(ninetyfive)
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "PSTI"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                     'period': period, 'value': float(PSTIndex)})
        result_cnn.commit()
        another_cursor.close()
    else:
        PSTIndex = "NULL"
    cnn.commit()
    cursor.close()
    return PSTIndex

#calculate site PLTI
def PLTI(siteTmp, startDate, endDate, result_set_id):      
    cursor = cnn.cursor()
    startDate = startDate + " 00:00:00"
    endDate = endDate + " 00:00:00"
    startDate = datetime.datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
    select_stmt = "SELECT flicker_plt_phase_a, flicker_plt_phase_b, flicker_plt_phase_c FROM continuous_flicker_plt " \
                  "WHERE site_id = %(site_id)s AND time_stamp >= %(start)s AND time_stamp < %(end)s"
    cursor.execute(select_stmt, {'site_id': siteTmp, 'start': startDate, 'end': endDate})
    row = cursor.fetchone()
    phaseA = []
    filtered_a = []
    phaseB = []
    filtered_b = []
    phaseC = []
    filtered_c = []
    ninetyfive = []
    while row is not None:  # store all the data first
        if row[0] != "NULL" and row[0] is not None:
            phaseA.append(float(row[0]))
        if row[1] != "NULL" and row[1] is not None:
            phaseB.append(float(row[1]))
        if row[2] != "NULL" and row[2] is not None:
            phaseC.append(float(row[2]))
        row = cursor.fetchone()
    if phaseA:
        filtered_a = filterFlicker(phaseA)
    if phaseB:
        filtered_b = filterFlicker(phaseB)
    if phaseC:
        filtered_c = filterFlicker(phaseC)
    if filtered_a:
        ninetyfive.append(np.percentile(filtered_a, 95))
    if filtered_b:
        ninetyfive.append(np.percentile(filtered_b, 95))
    if filtered_c:
        ninetyfive.append(np.percentile(filtered_c, 95))
    if ninetyfive:
        PLTIndex = max(ninetyfive)
        if (endDate - startDate).days > 31:
            period = "Annual"
        else:
            month = startDate.month
            if month == 1:
                period = "January"
            if month == 2:
                period = "February"
            if month == 3:
                period = "March"
            if month == 4:
                period = "April"
            if month == 5:
                period = "May"
            if month == 6:
                period = "June"
            if month == 7:
                period = "July"
            if month == 8:
                period = "August"
            if month == 9:
                period = "September"
            if month == 10:
                period = "October"
            if month == 11:
                period = "November"
            if month == 12:
                period = "December"
        index = "PLTI"
        another_cursor = result_cnn.cursor()
        insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                      "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
        another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                             'period': period, 'value': float(PLTIndex)})
        result_cnn.commit()
        another_cursor.close()
    else:
        PLTIndex = "NULL"
    cnn.commit()
    cursor.close()
    return PLTIndex

#calculate site CSI
def CSI(siteTmp, result_set_id):    #if cannot find the limit, use default limit    Annual
    cursor = result_cnn.cursor()
    select_stmt = "SELECT site_utility, voltage_level, nominal_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    result = cursor.fetchone()
    utility = result[0]
    level = result[1]
    nominalVoltage = result[2]        #for MV site to find the limit
    select_stmt = "SELECT utility_jurisdiction_id FROM utility WHERE utility_id = %(utility_id)s"
    cursor.execute(select_stmt, {'utility_id': utility})
    row = cursor.fetchone()
    juristiction = row[0]
    valid = 0              #store the number of valid indices
    valid_indices = []     #used to store the name of valid indices
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'VUFI' AND result_set_id = '2015-2016 Full Run' " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        VUFIndex = float(row[0])
        valid = valid + 1
        valid_indices.append("VUFI")     #stored in valid_indices[0]
    else:
        valid_indices.append("*")        #* is the mark for not valid indices
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'THDI' AND result_set_id = '2015-2016 Full Run' " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        THDIndex = float(row[0])
        valid = valid + 1
        valid_indices.append("THDI")     #stored in valid_indices[1]
    else:
        valid_indices.append("*")        #* is the mark for not valid indices
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'PSTI' AND result_set_id = '2015-2016 Full Run' " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        PSTIndex = float(row[0])
        valid = valid + 1
        valid_indices.append("PSTI")      #stored in valid_indices[2]
    else:
        valid_indices.append("*")         #* is the mark for not valid indices
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'PLTI' AND result_set_id = '2015-2016 Full Run' " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        PLTIndex = float(row[0])
        valid = valid + 1
        valid_indices.append("PLTI")     #stored in valid_indices[3]
    else:
        valid_indices.append("*")        #* is the mark for not valid indices
    select_stmt = "SELECT value FROM site_result WHERE index_id = 'SI' AND result_set_id = '2015-2016 Full Run' " \
                  "AND site_id = %(site_id)s AND period = 'Annual'"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    if row is not None:
        SIndex = float(row[0])
        valid = valid + 1
        valid_indices.append("SI")       #stored in valid_indices[4]
    else:
        valid_indices.append("*")        #* is the mark for not valid indices
    if level == "LV":                    #only LV site needs to calculate AVDI
        select_stmt = "SELECT value FROM site_result WHERE index_id = 'AVDI' AND result_set_id = '2015-2016 Full Run' " \
                      "AND site_id = %(site_id)s AND period = 'Annual'"
        cursor.execute(select_stmt, {'site_id': siteTmp})
        row = cursor.fetchone()
        if row is not None:
            AVDIndex = float(row[0])
            valid = valid + 1
            valid_indices.append("AVDI")  #stored in valid_indices[5]
        else:
            valid_indices.append("*")    # * is the mark for not valid indices
        # For LV sites a CSI value is only calculated if the site has at least 3 valid indices
        if valid < 3:
            return 0
        else:
            if valid_indices[5] != "*":    #AVDI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[5], 'voltage_level': level}) #limit for AVDI
                row = cursor.fetchall()
                if len(row) == 0:   # get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[5],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    AVDIlimit = float(default_limit)
                else:
                    for r in row:
                        AVDIlimit = float(r[0])
                AVDInorm = AVDIndex/AVDIlimit
            else:
                AVDInorm = 0

            if valid_indices[0] != "*":   #VUFI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[0], 'voltage_level': level})  # limit for VUFI
                row = cursor.fetchall()
                if len(row) == 0:   #get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[0],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    VUFIlimit = float(default_limit)
                else:
                    for r in row:
                        VUFIlimit = float(r[0])
                VUFInorm = pow(VUFIndex/VUFIlimit, 2)
            else:
                VUFInorm = 0

            if valid_indices[1] != "*":   #THDI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[1], 'voltage_level': level})  # limit for THDI
                row = cursor.fetchall()
                if len(row) == 0:           #get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[1],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    THDIlimit = float(default_limit)
                else:
                    for r in row:
                        THDIlimit = float(r[0])
                THDInorm = pow(THDIndex/THDIlimit, 2)
            else:
                THDInorm = 0

            if valid_indices[2] != "*":   #PSTI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[2], 'voltage_level': level})  # limit for PSTI
                row = cursor.fetchall()
                if len(row) == 0:   #get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[2],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    PSTIlimit = float(default_limit)
                else:
                    for r in row:
                        PSTIlimit = float(r[0])
                PSTInorm = pow(PSTIndex / PSTIlimit, 3)
            else:
                PSTInorm = 0

            if valid_indices[3] != "*":   #PLTI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[3], 'voltage_level': level})  # limit for PLTI
                row = cursor.fetchall()
                if len(row) == 0:   #get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[3],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    PLTIlimit = float(default_limit)
                else:
                    for r in row:
                        PLTIlimit = float(r[0])
                PLTInorm = pow(PLTIndex / PLTIlimit, 3)
            else:
                PLTInorm = 0

            Pnorm = max(PLTInorm, PSTInorm)

            if valid_indices[4] != "*":   #SI is valid
                select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[4], 'voltage_level': level})  # limit for SI
                row = cursor.fetchall()
                if len(row) == 0:   #get default limit
                    default_juristiction = 1
                    select_stmt = "SELECT value FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[4],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    for r in row:
                        default_limit = r[0]
                    SIlimit = float(default_limit)
                else:
                    for r in row:
                        SIlimit = float(r[0])
                SInorm = SIndex/SIlimit
            else:
                SInorm = 0

    if level == "MV":
        # For MV sites a CSI value is only calculated if the site has at least 2 valid indices
        if valid < 2:
            return 0
        else:
            AVDInorm = 0
            if valid_indices[0] != "*":   #VUFI is valid
                limit = []  # used for store limit
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[0],
                                             'nominal_voltage': nominalVoltage, 'voltage_level': level})  # limit for VUFI
                row = cursor.fetchall()
                if len(row) == 0:         #find the limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[0],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:      #find the default limit
                        default_juristiction = 1
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                  "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[0],
                                                     'nominal_voltage': nominalVoltage, 'voltage_level': level})
                        row = cursor.fetchall()
                        if len(row) == 0:    #find the default limit for nominal voltage is NULL
                            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                          "voltage_level = %(voltage_level)s"
                            cursor.execute(select_stmt,
                                           {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[0],
                                            'voltage_level': level})
                            row = cursor.fetchall()
                            for r in row:        #has limit for default juristiction id and nominal voltage is NULL
                                default_limit = float(r[0])
                            VUFIlimit = default_limit
                        else:   #has limit for default juristiction id and nominal voltage is not NULL
                            for r in row:
                                limit.append(float(r[0]))
                            i = len(limit)
                            VUFIlimit = limit[i - 1]
                    else:        #has limit for this juristiction id and nominal voltage is NULL
                        for r in row:
                            default_limit = float(r[0])
                        VUFIlimit = default_limit
                else:            #has limit for this juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    VUFIlimit = limit[i-1]  #get the limit whose nominal voltage is biggest but smaller than site nominal voltage
                VUFInorm = pow(VUFIndex/VUFIlimit, 2)
            else:
                VUFInorm = 0

            if valid_indices[1] != "*":   #THDI is valid
                limit = []  # used for store limit
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[1],
                                             'nominal_voltage': nominalVoltage,
                                             'voltage_level': level})  # limit for THDI
                row = cursor.fetchall()
                if len(row) == 0:  # find the limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[1],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit
                        default_juristiction = 1
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[1],
                                        'nominal_voltage': nominalVoltage, 'voltage_level': level})
                        row = cursor.fetchall()
                        if len(row) == 0:  # find the default limit for nominal voltage is NULL
                            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                          "voltage_level = %(voltage_level)s"
                            cursor.execute(select_stmt, {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[1],
                                            'voltage_level': level})
                            row = cursor.fetchall()
                            for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                                default_limit = float(r[0])
                            THDIlimit = default_limit
                        else:  # has limit for default juristiction id and nominal voltage is not NULL
                            for r in row:
                                limit.append(float(r[0]))
                            i = len(limit)
                            THDIlimit = limit[i - 1]
                    else:  # has limit for this juristiction id and nominal voltage is NULL
                        for r in row:
                            default_limit = float(r[0])
                            THDIlimit = default_limit
                else:  # has limit for this juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    THDIlimit = limit[i - 1]
                    # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
                THDInorm = pow(THDIndex/THDIlimit, 2)
            else:
                THDInorm = 0

            if valid_indices[2] != "*":   #PSTI is valid
                limit = []  # used for store limit
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[2],
                                             'nominal_voltage': nominalVoltage,
                                             'voltage_level': level})  # limit for PSTI
                row = cursor.fetchall()
                if len(row) == 0:  # find the limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[2],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit
                        default_juristiction = 1
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[2],
                                        'nominal_voltage': nominalVoltage, 'voltage_level': level})
                        row = cursor.fetchall()
                        if len(row) == 0:  # find the default limit for nominal voltage is NULL
                            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                          "voltage_level = %(voltage_level)s"
                            cursor.execute(select_stmt,
                                           {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[2],
                                            'voltage_level': level})
                            row = cursor.fetchall()
                            for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                                default_limit = float(r[0])
                            PSTIlimit = default_limit
                        else:  # has limit for default juristiction id and nominal voltage is not NULL
                            for r in row:
                                limit.append(float(r[0]))
                            i = len(limit)
                            PSTIlimit = limit[i - 1]
                    else:  # has limit for this juristiction id and nominal voltage is NULL
                        for r in row:
                            default_limit = float(r[0])
                            PSTIlimit = default_limit
                else:  # has limit for this juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    PSTIlimit = limit[i - 1]
                    # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
                PSTInorm = pow(PSTIndex / PSTIlimit, 3)
            else:
                PSTInorm = 0
            if valid_indices[3] != "*":   #PLTI is valid
                limit = []  # used for store limit
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[3],
                                             'nominal_voltage': nominalVoltage,
                                             'voltage_level': level})  # limit for PLTI
                row = cursor.fetchall()
                if len(row) == 0:  # find the limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[3],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit
                        default_juristiction = 1
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[3],
                                        'nominal_voltage': nominalVoltage, 'voltage_level': level})
                        row = cursor.fetchall()
                        if len(row) == 0:  # find the default limit for nominal voltage is NULL
                            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                          "voltage_level = %(voltage_level)s"
                            cursor.execute(select_stmt,
                                           {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[3],
                                            'voltage_level': level})
                            row = cursor.fetchall()
                            for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                                default_limit = float(r[0])
                            PLTIlimit = default_limit
                        else:  # has limit for default juristiction id and nominal voltage is not NULL
                            for r in row:
                                limit.append(float(r[0]))
                            i = len(limit)
                            PLTIlimit = limit[i - 1]
                    else:  # has limit for this juristiction id and nominal voltage is NULL
                        for r in row:
                            default_limit = float(r[0])
                            PLTIlimit = default_limit
                else:  # has limit for this juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    PLTIlimit = limit[i - 1]
                    # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
                PLTInorm = pow(PLTIndex / PLTIlimit, 3)
            else:
                PLTInorm = 0

            Pnorm = max(PLTInorm, PSTInorm)

            if valid_indices[4] != "*":   #SI is valid
                limit = []  # used for store limit
                select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                              "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                              "voltage_level = %(voltage_level)s"
                cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[4],
                                             'nominal_voltage': nominalVoltage,
                                             'voltage_level': level})  # limit for SI
                row = cursor.fetchall()
                if len(row) == 0:  # find the limit for nominal voltage is NULL
                    select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                  "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                  "voltage_level = %(voltage_level)s"
                    cursor.execute(select_stmt, {'jurisdiction_id': juristiction, 'index_name': valid_indices[4],
                                                 'voltage_level': level})
                    row = cursor.fetchall()
                    if len(row) == 0:  # find the default limit
                        default_juristiction = 1
                        select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                      "index_name = %(index_name)s and nominal_voltage <= %(nominal_voltage)s and " \
                                      "voltage_level = %(voltage_level)s"
                        cursor.execute(select_stmt,
                                       {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[4],
                                        'nominal_voltage': nominalVoltage, 'voltage_level': level})
                        row = cursor.fetchall()
                        if len(row) == 0:  # find the default limit for nominal voltage is NULL
                            select_stmt = "SELECT value, nominal_voltage FROM jurisfictional_limits WHERE jurisdiction_id = %(jurisdiction_id)s and " \
                                          "index_name = %(index_name)s and nominal_voltage is NULL and " \
                                          "voltage_level = %(voltage_level)s"
                            cursor.execute(select_stmt,
                                           {'jurisdiction_id': default_juristiction, 'index_name': valid_indices[4],
                                            'voltage_level': level})
                            row = cursor.fetchall()
                            for r in row:  # has limit for default juristiction id and nominal voltage is NULL
                                default_limit = float(r[0])
                            SIlimit = default_limit
                        else:  # has limit for default juristiction id and nominal voltage is not NULL
                            for r in row:
                                limit.append(float(r[0]))
                            i = len(limit)
                            SIlimit = limit[i - 1]
                    else:  # has limit for this juristiction id and nominal voltage is NULL
                        for r in row:
                            default_limit = float(r[0])
                            SIlimit = default_limit
                else:  # has limit for this juristiction id and nominal voltage is not NULL
                    for r in row:
                        limit.append(float(r[0]))
                    i = len(limit)
                    SIlimit = limit[i - 1]
                    # get the limit whose nominal voltage is biggest but smaller than site nominal voltage
                SInorm = SIndex/SIlimit
            else:
                SInorm = 0
    CSIndex = (AVDInorm + VUFInorm + THDInorm + Pnorm + SInorm)/valid
    period = "Annual"
    index = "CSI"
    another_cursor = result_cnn.cursor()
    insert_stmt = "INSERT INTO site_result (result_set_id, site_id, index_id, period, value) " \
                  "VALUES (%(result_set_id)s, %(site_id)s, %(index_id)s, %(period)s, %(value)s)"
    another_cursor.execute(insert_stmt, {'result_set_id': result_set_id, 'site_id': siteTmp, 'index_id': index,
                                         'period': period, 'value': float(CSIndex)})
    result_cnn.commit()
    another_cursor.close()
    del valid_indices
    cnn.commit()
    cursor.close()
    return CSIndex