from database_connections import *
import mysql.connector
from mysql.connector import errorcode

def filterVoltage(normalized, siteTmp):
    cursor = cnn.cursor()
    select_stmt = "SELECT mor_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    row = cursor.fetchone()
    mor = row[0]   # mor is the site middle of the range voltage value from site database
    cnn.commit()
    cursor.close()
    filtered = []
    for voltage in normalized:
        if voltage / mor <= 1.2 and voltage / mor >= 0.8:
            filtered.append(voltage)
    return filtered

def filterUnbalance(normalized):
    filtered = []
    for unbalance in normalized:
        if unbalance < 10:
            filtered.append(unbalance)
    return filtered

def filterHarmonic_percentage(normalized):   #uom of harmonic should be percentage
    filtered = []
    for h in normalized:
        if h > 0 and h < 12:
            filtered.append(h)
    return filtered

def filterHarmonic(harmonic, siteTmp):   #uom of harmonic should be voltage  
    cursor = another_cnn.cursor()
    select_stmt = "SELECT reference_voltage FROM site WHERE site_id = %(site_id)s"
    cursor.execute(select_stmt, {'site_id': siteTmp})
    reference = cursor.fetchone()
    h = harmonic / reference             # convert to percentage
    another_cnn.commit()
    cursor.close()
    if h <= 0 or h >= 12:
        return False
    else:
        return True

def filterSag(retainedVoltage, dur):
    if retainedVoltage <= 0 or retainedVoltage >= 0.9 or dur < 0.01 or dur >= 60:
        return False
    else:
        return True

def filterFlicker(flicker):
    filtered = []
    for value in flicker:
        if value > 0 and value < 10:
            filtered.append(value)
    return filtered